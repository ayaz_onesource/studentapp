package com.onesourceweb.student.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.onesourceweb.student.BaseActivity;
import com.onesourceweb.student.R;
import com.onesourceweb.student.adpater.TabPagerAdapter;
import com.onesourceweb.student.fragment.CorrectAnswerFragment;
import com.onesourceweb.student.fragment.InCorrectAnswerFragment;
import com.onesourceweb.student.fragment.NotAttempAnswerFragment;
import com.onesourceweb.student.model.TestDetails;
import com.onesourceweb.student.model.TestSummaryBean;
import com.onesourceweb.student.network.NetworkRequest;
import com.onesourceweb.student.utils.Constant;
import com.onesourceweb.student.utils.L;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;

public class TestSummaryActivity extends BaseActivity {

    private static final String TAG = "TestSummaryActivity";
    TabLayout tabLayout;
    ViewPager viewPager;
    private ImageView mImgBackSummary;

    String id;
    private TextView mProgTvC;
    private ProgressBar mProgCorrect;
    private TextView mProgTvInc;
    private ProgressBar mProgIncorrect;
    private TextView mProgTvNta;
    private ProgressBar mProgNotans;

    Subscription subscription;
    TestSummaryBean testSummaryBean;

    char a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_summary);
        id = getIntent().getStringExtra(Constant.PAPERID);

        viewPager = (ViewPager) findViewById(R.id.summary_pager);
        tabLayout = (TabLayout) findViewById(R.id.summary_tab);


        initView();
        if (L.isNetworkAvailable(this)) {
            callTestSummary();
        }

        // summaryCurrentModels = new ArrayList<>();

        mImgBackSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(TestSummaryActivity.this, TestReportActivity.class));
//                finish();
                onBackPressed();
            }
        });


    }

    private void callTestSummary() {

        Map<String, String> map = new HashMap<>();
        map.put(Constant.PAPERID, id);

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getTestSummary(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    Log.d(TAG, "testsumry" + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject("data");

                    testSummaryBean= LoganSquare.parse(DATA.toString(),TestSummaryBean.class);

                    TestDetails testDetails=testSummaryBean.getTestDetails();
//                    JSONObject D = DATA.getJSONObject("test_details");
//
//                    int total = D.getInt("TotalQuestion");
                    int noTans = (testDetails.getTotalQuestion() -testDetails.getTotalAttempt());
//                    String correct = D.getString("TotalRight");
//                    String incorrect = D.getString("TotalWrong");
//
                    mProgTvC.setText(testDetails.getTotalRight() + "-" + testDetails.getTotalQuestion());
                    mProgTvInc.setText(testDetails.getTotalWrong() + "-" + testDetails.getTotalQuestion());
                    mProgTvNta.setText("" + noTans + "-" + testDetails.getTotalQuestion());

                    mProgCorrect.setMax(testDetails.getTotalQuestion());
                    mProgCorrect.setProgress(testDetails.getTotalRight());
                    mProgIncorrect.setMax(testDetails.getTotalQuestion());
                    mProgIncorrect.setProgress(testDetails.getTotalWrong());
                    mProgNotans.setMax(testDetails.getTotalQuestion());
                    mProgNotans.setProgress(noTans);

//
//                    JSONArray correctdata = DATA.getJSONArray("corret_questions");
//                    JSONArray incorretdata = DATA.getJSONArray("incorret_questions");
//                    JSONArray notappeareddata = DATA.getJSONArray("not_appeared_questions");
//
//
//                    if (correctdata.length() != 0) {
//
//                        for (int i = 0; i < correctdata.length(); i++) {
//
//                            JSONObject O = correctdata.getJSONObject(i);
//                            JSONArray option = O.getJSONArray("Options");
//
//                            List<SummaryOptionModel> summaryOptionModels = new ArrayList<>();
//
//                            for (int j = 0; j < option.length(); j++) {
//
//                                JSONObject op = option.getJSONObject(j);
//                                SummaryOptionModel summaryOptionModel = new SummaryOptionModel("A", op.getString("Options"), op.getString("IsCorrect"), op.getString("MCQOptionID"));
//                                summaryOptionModels.add(summaryOptionModel);
//
//                            }
//
//                            SummaryCurrentModel summaryCurrentModel = new SummaryCurrentModel(O.getString("Question"), i + 1, " ", summaryOptionModels);
//                            summaryCurrentModelscor.add(summaryCurrentModel);
//
//                        }
//                    }
//                    if (incorretdata.length() != 0) {
//
//                        for (int i = 0; i < incorretdata.length(); i++) {
//
//                            JSONObject O = incorretdata.getJSONObject(i);
//                            JSONArray option = O.getJSONArray("Options");
//
//                            List<SummaryOptionModel> summaryOptionModels = new ArrayList<>();
//
//                            for (int j = 0; j < option.length(); j++) {
//                                JSONObject op = option.getJSONObject(j);
//                                SummaryOptionModel summaryOptionModel = new SummaryOptionModel("A", op.getString("Options"), op.getString("IsCorrect"), op.getString("MCQOptionID"));
//                                summaryOptionModels.add(summaryOptionModel);
//                            }
//
//                            SummaryCurrentModel summaryCurrentModel = new SummaryCurrentModel(O.getString("Question"), i + 1, O.getString("AnswerID"), summaryOptionModels);
//                            summaryCurrentModelsincor.add(summaryCurrentModel);
//                        }
//                    }
//                    if (notappeareddata.length() != 0) {
//                        for (int i = 0; i < notappeareddata.length(); i++) {
//
//                            JSONObject O = notappeareddata.getJSONObject(i);
//                            JSONArray option = O.getJSONArray("Options");
//
//                            List<SummaryOptionModel> summaryOptionModels = new ArrayList<>();
//
//                            for (int j = 0; j < option.length(); j++) {
//
//                                JSONObject op = option.getJSONObject(j);
//                                SummaryOptionModel summaryOptionModel = new SummaryOptionModel("A", op.getString("Options"), op.getString("IsCorrect"), op.getString("MCQOptionID"));
//                                summaryOptionModels.add(summaryOptionModel);
//
//                            }
//
//                            SummaryCurrentModel summaryCurrentModel = new SummaryCurrentModel(O.getString("Question"), i + 1, "", summaryOptionModels);
//                            summaryCurrentModelsnot.add(summaryCurrentModel);
//                        }
//
//                    }

                    setupViewPager(viewPager);
                    viewPager.setOffscreenPageLimit(3);
                    tabLayout.setupWithViewPager(viewPager);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        TabPagerAdapter viewPagerAdapter = new TabPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragment(CorrectAnswerFragment.newInstance(testSummaryBean.getCorretQuestions()), "Correct");
        viewPagerAdapter.addFragment(InCorrectAnswerFragment.newInstance(testSummaryBean.getIncorretQuestions()), "In Correct");
        viewPagerAdapter.addFragment(NotAttempAnswerFragment.newInstance(testSummaryBean.getNotAppearedQuestions()), "Not Attempted");

        viewPager.setAdapter(viewPagerAdapter);
    }


    private void initView() {
        mImgBackSummary = (ImageView) findViewById(R.id.img_back_summary);
        mProgTvC = (TextView) findViewById(R.id.prog_tv_c);
        mProgCorrect = (ProgressBar) findViewById(R.id.prog_correct);
        mProgTvInc = (TextView) findViewById(R.id.prog_tv_inc);
        mProgIncorrect = (ProgressBar) findViewById(R.id.prog_incorrect);
        mProgTvNta = (TextView) findViewById(R.id.prog_tv_nta);
        mProgNotans = (ProgressBar) findViewById(R.id.prog_notans);
    }
}
