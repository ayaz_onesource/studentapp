package com.onesourceweb.student.adpater;

import android.content.Context;
import android.text.Html;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.onesourceweb.student.R;
import com.onesourceweb.student.model.LegalDataModel;

import java.util.List;

public class LegalListviewAdapater extends BaseAdapter {

    private final Context mContext;
    private final SparseBooleanArray mCollapsedStatus;
    private List<LegalDataModel> legalDataModels;

    public LegalListviewAdapater(Context context) {
        mContext = context;
        mCollapsedStatus = new SparseBooleanArray();
        //mContext.getResources().getStringArray(R.array.sampleStrings);
    }

    public LegalListviewAdapater(Context context, List<LegalDataModel> legalDataModels) {
        mContext = context;
        this.legalDataModels = legalDataModels;
        mCollapsedStatus = new SparseBooleanArray();
    }

    @Override
    public int getCount() {
        return legalDataModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_group, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.expandableTextView = (ExpandableTextView) convertView.findViewById(R.id.expand_text_view);
            viewHolder.tittle = (TextView) convertView.findViewById(R.id.title);
            viewHolder.legelrow = (LinearLayout) convertView.findViewById(R.id.legelrow);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (position == 0) {
            viewHolder.legelrow.setBackgroundResource(R.drawable.firstpos);
        } else if (position == getCount() - 1)
            viewHolder.legelrow.setBackgroundResource(R.drawable.lastpos);

        viewHolder.tittle.setText(legalDataModels.get(position).getTittle());
        viewHolder.expandableTextView.setText(Html.fromHtml(legalDataModels.get(position).getSubdata()), mCollapsedStatus, position);

        return convertView;
    }


    private static class ViewHolder {
        ExpandableTextView expandableTextView;
        TextView tittle;
        LinearLayout legelrow;
    }
}