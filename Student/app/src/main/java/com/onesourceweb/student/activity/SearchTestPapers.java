package com.onesourceweb.student.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.onesourceweb.student.BaseActivity;
import com.onesourceweb.student.List_Onclick;
import com.onesourceweb.student.R;
import com.onesourceweb.student.adpater.SearchPaperAdpater;
import com.onesourceweb.student.fragment.BottomSheetFragment;
import com.onesourceweb.student.model.SearchPaperModel;
import com.onesourceweb.student.network.NetworkRequest;
import com.onesourceweb.student.utils.Constant;
import com.onesourceweb.student.utils.L;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Subscription;

public class SearchTestPapers extends BaseActivity implements List_Onclick {


    private static final String TAG = "SearchTestPapers";
    private RecyclerView mMstudentList;

    SearchPaperAdpater searchPaperAdpater;
    List<SearchPaperModel> searchPaperModels;
    private ImageView mImgFilter;

    Subscription subscription;
    private ImageView mImgBackAssignment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_test_papers);
        initView();


        searchPaperModels = new ArrayList<>();

        if (L.isNetworkAvailable(SearchTestPapers.this))
            callpaperlist();

        mMstudentList.setHasFixedSize(true);
        mMstudentList.setLayoutManager(new LinearLayoutManager(SearchTestPapers.this));
        mMstudentList.setItemAnimator(new DefaultItemAnimator());

        mImgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();
                bottomSheetFragment.setCancelable(true);
                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
            }
        });
        mImgBackAssignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SearchTestPapers.this, MainActivity.class));
                finish();
            }
        });

    }

    private void callpaperlist() {

        Map<String, String> map = new HashMap<>();
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getsearchpaper(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    Log.d(TAG, "paperlist" + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONArray DATA = jsonResponse.getJSONArray("data");

                    for (int i = 0; i < DATA.length(); i++) {

                        JSONObject O = DATA.getJSONObject(i);

                        String StudentMCQTestID = O.getString("StudentMCQTestID");
                        String SubjectName = (O.getString("SubjectName"));
                        String SubmitDate = (O.getString("SubmitDate"));
                        String TotalRight = (O.getString("TotalRight"));
                        String TotalQuestion = (O.getString("TotalQuestion"));
                        String MCQPlannerID = (O.getString("MCQPlannerID"));

                        SearchPaperModel searchPaperModel = new SearchPaperModel(StudentMCQTestID, SubjectName, SubmitDate, TotalRight, TotalQuestion, MCQPlannerID);
                        searchPaperModels.add(searchPaperModel);
                    }
//                    searchPaperModels.add((SearchPaperModel) sm);
                    searchPaperAdpater = new SearchPaperAdpater(SearchTestPapers.this, searchPaperModels);
                    mMstudentList.setAdapter(searchPaperAdpater);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }

    private void initView() {
        mMstudentList = (RecyclerView) findViewById(R.id.mstudentList);
        mImgFilter = (ImageView) findViewById(R.id.img_filter);
        mImgBackAssignment = (ImageView) findViewById(R.id.img_back_assignment);
    }

    @Override
    public void clicklist(int id) {

        Intent i = new Intent(SearchTestPapers.this, TestReportActivity.class);
        i.putExtra(Constant.PAPERID, id);
        startActivity(i);
    }
}
