package com.onesourceweb.student.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onesourceweb.student.R;
import com.onesourceweb.student.adpater.CorrectListAdpater;
import com.onesourceweb.student.model.QuestionsBean;
import com.onesourceweb.student.model.SummaryCurrentModel;

import java.io.Serializable;
import java.util.List;

public class NotAttempAnswerFragment extends Fragment {

    List<QuestionsBean> summaryCurrentModels;
    private static final String TAG = "NotAttempAnswerFragment";

    public static Fragment newInstance(List<QuestionsBean> summaryCurrentModel) {

        NotAttempAnswerFragment notAttempAnswerFragment = new NotAttempAnswerFragment();
        Bundle args = new Bundle();
        args.putSerializable("notans", (Serializable) summaryCurrentModel);
        notAttempAnswerFragment.setArguments(args);
        Log.d(TAG, "bundle data: " + args.size());

        return notAttempAnswerFragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_correct_answer, container, false);
        summaryCurrentModels = (List<QuestionsBean>) getArguments().getSerializable("notans");
        RecyclerView mMstudentList = (RecyclerView) layoutView.findViewById(R.id.mstudentList);
        mMstudentList.setHasFixedSize(true);
        mMstudentList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mMstudentList.setItemAnimator(new DefaultItemAnimator());
        mMstudentList.setAdapter(new CorrectListAdpater(getActivity(), summaryCurrentModels));

        return layoutView;
    }

}
