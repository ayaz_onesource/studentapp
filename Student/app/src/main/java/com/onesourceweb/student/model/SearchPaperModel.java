package com.onesourceweb.student.model;

public class SearchPaperModel {

    String StudentMCQTestID, SubjectName, SubmitDate, TotalRight, TotalQuestion, MCQPlannerID;


    public SearchPaperModel(String studentMCQTestID, String subjectName, String submitDate, String totalRight, String totalQuestion, String MCQPlannerID) {
        StudentMCQTestID = studentMCQTestID;
        SubjectName = subjectName;
        SubmitDate = submitDate;
        TotalRight = totalRight;
        TotalQuestion = totalQuestion;
        this.MCQPlannerID = MCQPlannerID;
    }

    public String getStudentMCQTestID() {
        return StudentMCQTestID;
    }

    public void setStudentMCQTestID(String studentMCQTestID) {
        StudentMCQTestID = studentMCQTestID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getSubmitDate() {
        return SubmitDate;
    }

    public void setSubmitDate(String submitDate) {
        SubmitDate = submitDate;
    }

    public String getTotalRight() {
        return TotalRight;
    }

    public void setTotalRight(String totalRight) {
        TotalRight = totalRight;
    }

    public String getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(String totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public String getMCQPlannerID() {
        return MCQPlannerID;
    }

    public void setMCQPlannerID(String MCQPlannerID) {
        this.MCQPlannerID = MCQPlannerID;
    }
}
