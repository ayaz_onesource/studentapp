package com.onesourceweb.student.adpater;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.onesourceweb.student.OptionsWebView;
import com.onesourceweb.student.R;
import com.onesourceweb.student.model.AssginmentModel;
import com.onesourceweb.student.model.SubmitTest;
import com.onesourceweb.student.model.TestModel;
import com.onesourceweb.student.model.TestOptionListModel;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class TestAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;

    List<TestOptionListModel> testOptionListModels;
    TestModel testModels;
    List<SubmitTest> submitTests = new ArrayList<>();


    int selectedPosition = -1;

    public TestAdpater(Context context, List<TestOptionListModel> testOptionListModels, TestModel testModels) {
        this.context = context;
        this.testOptionListModels = testOptionListModels;
        this.testModels = testModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_row_item_option, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderIn, final int position) {

        final ViewHolder holder = (ViewHolder) holderIn;
        TestOptionListModel testOptionListModel = testOptionListModels.get(position);


        Log.d(TAG, "testdata: " + testOptionListModel.getOptionText());

        if (testOptionListModel.getIsslected()) {
            if (position == 0) {
                holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.selectfirstpos));
                holder.index_layout.setBackground(ContextCompat.getDrawable(context, R.drawable.select_rounded_corner_bg));
                //  holder.mTvoption.setTextColor((Color.WHITE));
                holder.mTvqid.setTextColor(Color.parseColor("#FA605F"));

            } else if (position == getItemCount() - 1) {
                holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.selectlastpos));
                holder.index_layout.setBackground(ContextCompat.getDrawable(context, R.drawable.select_rounded_corner_bg));
                holder.mTvqid.setTextColor(Color.parseColor("#FA605F"));

            } else {

                holder.rowmainlayout.setBackgroundResource(R.drawable.gardiant);
                holder.index_layout.setBackground(ContextCompat.getDrawable(context, R.drawable.select_rounded_corner_bg));
                holder.mTvqid.setTextColor(Color.parseColor("#FA605F"));
            }
        } else {
            if (position == 0) {
                holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.firstpos));
            } else if (position == getItemCount() - 1) {
                holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.lastpos));
                // holder.mTvoption.setTextColor((Color.BLACK));
            } else {
                holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.defult_pos));

            }
        }


        holder.mTvqid.setText(String.valueOf((char) (65 + position)));

        holder.mTvoption.loadHtmlFromLocal(testOptionListModel.getOptionText());
        //holder.mTvoption.setInitialScale(40);

        // holder.mTvoption.setBackgroundColor(Color.TRANSPARENT);
        // holder.mTvoption.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        holder.mTvoption.setScrollbarFadingEnabled(false);
        // holder.mTvoption.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);


        holder.rowmainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPosition != -1) {
                    testOptionListModels.get(selectedPosition).setIsslected(false);
                }
                testOptionListModels.get(position).setIsslected(true);
                selectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();

            }
        });
    }

    @Override
    public int getItemCount() {
        return testOptionListModels.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvqid;
        private OptionsWebView mTvoption;
        private LinearLayout rowmainlayout;
        private LinearLayout index_layout;

        public ViewHolder(View v) {
            super(v);
            mTvqid = (TextView) v.findViewById(R.id.tv_qid);
            mTvoption = (OptionsWebView) v.findViewById(R.id.tv_option);
            rowmainlayout = (LinearLayout) v.findViewById(R.id.rowmainlayout1);
            index_layout = (LinearLayout) v.findViewById(R.id.index_layout);

        }

    }

}
