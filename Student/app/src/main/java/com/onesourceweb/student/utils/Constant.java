package com.onesourceweb.student.utils;

public class Constant {
    public static final String StudentCode = "StudentCode";
    public static final String DeviceID = "DeviceID";
    public static final String PlayerID = "PlayerID";
    public static boolean isAlertShow = false;
    public static String message = "message";
    public static String isLogin = "isLogin";
    public static String Otp = "OTP";
    public static String MobileNo = "mobileno";
    public static String loginAuthToken = "loginAuthToken";
    public static String USERID = "id";
    public static String PAPERID = "PaperID";
    public static String MCQPlannerID = "MCQPlannerID";
    public static String Imgfullpath = "folder_path";


}
