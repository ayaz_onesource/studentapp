package com.onesourceweb.student.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.onesourceweb.student.BaseActivity;
import com.onesourceweb.student.R;
import com.onesourceweb.student.network.NetworkRequest;
import com.onesourceweb.student.utils.Constant;
import com.onesourceweb.student.utils.L;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import rx.Subscription;

public class ProfileActivity extends BaseActivity {

    private static final String TAG = "ProfileActivity";
    private ImageView mImgBackReport, mImgEdit;
    private CircleImageView mProfileImage;
    private TextView mTvusername, mTvUmob;
    private ProgressBar mProgressBar;
    private MaterialEditText mEdtUserid, mEdtEmailid, mEdtMobileno, mEdtGreno, mEdt, mEdtMed, mEdtStd, mEdtBranch, mEdtBatch;


    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initView();

        if (L.isNetworkAvailable(ProfileActivity.this)) {
            callProfile();
        }

        mImgEdit.setOnClickListener(new View.OnClickListener() {

            private boolean select = false;

            public void onClick(View v) {
                if (select == true) {
                    mImgEdit.setImageResource(R.drawable.edit);
                    mEdtUserid.setEnabled(false);
                    mEdtEmailid.setEnabled(false);
                    select = false;
                } else {
                    select = true;
                    mImgEdit.setImageResource(R.drawable.check);
                    mEdtUserid.setEnabled(true);
                    mEdtEmailid.setEnabled(true);
                }
            }
        });

        mImgBackReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this, MainActivity.class));
                finish();
            }
        });
    }

    private void callProfile() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getProfile(), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    Log.d(TAG, "profilelogin: " + data.body());

                    JSONObject profdata = jsonResponse.getJSONObject("data");

                    mTvusername.setText(((profdata.getString("firstname")) + " " + (profdata.getString("lastname"))));
                    mTvUmob.setText(profdata.getString("username"));
                    mEdtUserid.setText((profdata.getString("firstname")) + " " + (profdata.getString("lastname")));
                    mEdtEmailid.setText(profdata.getString("email"));
                    mEdtMobileno.setText(profdata.getString("mobileno"));
                    mEdtGreno.setText(profdata.getString("BoardName"));
                    mEdtMed.setText(profdata.getString("MediumName"));
                    mEdtStd.setText(profdata.getString("StandardName"));
                    mEdtBranch.setText(profdata.getString("BranchName"));
                    mEdtBatch.setText(profdata.getString("BatchName"));


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }

    private void initView() {
        mImgBackReport = (ImageView) findViewById(R.id.img_back_report);
        mProfileImage = (CircleImageView) findViewById(R.id.profile_image);
        mTvusername = (TextView) findViewById(R.id.mTvusername);
        mTvUmob = (TextView) findViewById(R.id.mTvUmob);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mImgEdit = (ImageView) findViewById(R.id.img_edit);
        mEdtUserid = (MaterialEditText) findViewById(R.id.edt_userid);
        mEdtEmailid = (MaterialEditText) findViewById(R.id.edt_emailid);
        mEdtMobileno = (MaterialEditText) findViewById(R.id.edt_mobileno);
        mEdtGreno = (MaterialEditText) findViewById(R.id.edt_greno);
        mEdt = (MaterialEditText) findViewById(R.id.edt);
        mEdtMed = (MaterialEditText) findViewById(R.id.edt_med);
        mEdtStd = (MaterialEditText) findViewById(R.id.edt_std);
        mEdtBranch = (MaterialEditText) findViewById(R.id.edt_branch);
        mEdtBatch = (MaterialEditText) findViewById(R.id.edt_batch);
    }


}
