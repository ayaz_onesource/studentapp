package com.onesourceweb.student.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.onesourceweb.student.BaseActivity;
import com.onesourceweb.student.BuildConfig;
import com.onesourceweb.student.R;
import com.onesourceweb.student.network.NetworkRequest;
import com.onesourceweb.student.utils.Constant;
import com.onesourceweb.student.utils.L;
import com.onesourceweb.student.utils.Validation;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;


public class LoginActivity extends BaseActivity {

    private static final String TAG = "LoginActivity";
    Button btnstudent, btnmobile, btnsubmit;
    TextView txt_title;
    EditText edttext;

    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_student);

        adddata();

        if (BuildConfig.DEBUG)
            edttext.setText("S0025-05");

        btnstudent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                btnstudent.setBackgroundResource(R.drawable.sub_select);
                btnmobile.setBackgroundResource(R.drawable.unselect_level);
                btnmobile.setTextColor(Color.parseColor("#DD4C4C"));
                btnstudent.setTextColor(Color.parseColor("#ffffff"));
                edttext.setInputType(InputType.TYPE_CLASS_TEXT);
                edttext.getText().clear();
                txt_title.setText("Please enter your Student ID");


            }
        });
        btnmobile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                btnstudent.setBackgroundResource(R.drawable.unselect);
                btnmobile.setBackgroundResource(R.drawable.select);
                edttext.getText().clear();
                edttext.setInputType(InputType.TYPE_CLASS_NUMBER);
                btnstudent.setTextColor(Color.parseColor("#DD4C4C"));
                btnmobile.setTextColor(Color.parseColor("#ffffff"));
                txt_title.setText("Please enter your Mobile NO");
            }
        });

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validation.isEmpty(edttext.getText().toString().trim())) {
                    edttext.setError("Please enter Student Id");
                } else {
                    if (L.isNetworkAvailable(LoginActivity.this))
                        callLogin();
                }
            }
        });

    }

    public void callLogin() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.StudentCode, L.getEditText(edttext));
        map.put(Constant.DeviceID, L.getDeviceId(this));
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLogin(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
//                    Log.d(TAG, "callLogin: " + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject phone = jsonResponse.getJSONObject("data");
                    String otp = phone.getString("otp");
                    String mobileno = phone.getString("mobileno");

                    Intent intent = new Intent(this, Verification.class);
                    intent.putExtra(Constant.Otp, otp);
                    intent.putExtra(Constant.MobileNo, mobileno);
                    intent.putExtra(Constant.StudentCode, edttext.getText().toString().trim());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }

    public void adddata() {
        btnstudent = findViewById(R.id.btn_student);
        btnmobile = findViewById(R.id.btn_mobile);
        btnsubmit = findViewById(R.id.btn_submit);
        txt_title = findViewById(R.id.txt_title);
        edttext = findViewById(R.id.edt_id);
    }


}
