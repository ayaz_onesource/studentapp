package com.onesourceweb.student.adpater;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onesourceweb.student.List_Onclick;
import com.onesourceweb.student.R;
import com.onesourceweb.student.activity.Assginment;
import com.onesourceweb.student.model.AssginmentModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AssginmentAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<AssginmentModel> assginmentModelslist;
    List_Onclick click;

    public AssginmentAdpater(Assginment assginment, List<AssginmentModel> assginmentModels) {
        this.context = assginment;
        this.assginmentModelslist = assginmentModels;
        this.click = assginment;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_row_item, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        AssginmentModel assginmentModel = assginmentModelslist.get(position);
        if (position == 0)
            holder.rowmainlayout.setBackgroundResource(R.drawable.firstpos);
        else if (position == getItemCount() - 1)
            holder.rowmainlayout.setBackgroundResource(R.drawable.lastpos);

        //  Log.d(TAG, "onBindViewHolder: "+notificationsBean.getProfileImgPath());
        // L.loadImageWithPicassoRound(notificationsBean.getProfileImgPath(), holder.mUserImage, mContext, 0, 0, holder.mProgress);

        holder.tvasignnname.setText(assginmentModel.getSubjectName());
        holder.tvdate.setText((assginmentModel.getStartDate()));

        holder.rowmainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                click.clicklist(assginmentModel.getMCQPlannerID());
            }
        });
    }

    @Override
    public int getItemCount() {
        return assginmentModelslist.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgdate)
        ImageView imgdate;
        @BindView(R.id.tvasignnname)
        TextView tvasignnname;
        @BindView(R.id.tvdate)
        TextView tvdate;
        @BindView(R.id.mItemView)
        LinearLayout mItemView;
        @BindView(R.id.rowmainlayout)
        LinearLayout rowmainlayout;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
