package com.onesourceweb.student;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.onesourceweb.student.network.RestAPIBuilder;
import com.onesourceweb.student.network.RestApi;
import com.onesourceweb.student.utils.Prefs;

public class BaseActivity extends AppCompatActivity {

    protected RestApi restApi;
    ProgressDialog progressDialog;
    public Prefs prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restApi = RestAPIBuilder.buildRetrofitService();
        prefs = Prefs.with(this);
    }


    public void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
        }
        if (isShow && !progressDialog.isShowing()) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }
}
