package com.onesourceweb.student.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class TestDetails implements Serializable {

    /*
    "StudentMCQTestID": "1",
      "StudentID": "16",
      "PlannerID": "1",
      "TotalQuestion": "3",
      "TotalRight": "2",
      "TotalWrong": "1",
      "TotalAttempt": "3",
      "Accuracy": "67",
      "SubmitDate": "2019-01-23 11:41:09"
     */
    @JsonField
    int StudentMCQTestID;
    @JsonField
    int StudentID;
    @JsonField
    int PlannerID;
    @JsonField
    int TotalQuestion;
    @JsonField
    int TotalRight;
    @JsonField
    int TotalWrong;
    @JsonField
    int TotalAttempt;
    @JsonField
    int Accuracy;
    @JsonField
    String SubmitDate;

    public int getStudentMCQTestID() {
        return StudentMCQTestID;
    }

    public void setStudentMCQTestID(int studentMCQTestID) {
        StudentMCQTestID = studentMCQTestID;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public int getPlannerID() {
        return PlannerID;
    }

    public void setPlannerID(int plannerID) {
        PlannerID = plannerID;
    }

    public int getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public int getTotalRight() {
        return TotalRight;
    }

    public void setTotalRight(int totalRight) {
        TotalRight = totalRight;
    }

    public int getTotalWrong() {
        return TotalWrong;
    }

    public void setTotalWrong(int totalWrong) {
        TotalWrong = totalWrong;
    }

    public int getTotalAttempt() {
        return TotalAttempt;
    }

    public void setTotalAttempt(int totalAttempt) {
        TotalAttempt = totalAttempt;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }

    public String getSubmitDate() {
        return SubmitDate;
    }

    public void setSubmitDate(String submitDate) {
        SubmitDate = submitDate;
    }
}
