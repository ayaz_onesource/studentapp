package com.onesourceweb.student.network;

import android.support.annotation.NonNull;

import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import rx.Observable;

public interface RestApi {

    @FormUrlEncoded
    @POST("student/login")
    Observable<Response<String>> getLogin(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/confirmotp")
    Observable<Response<String>> getverification(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/Paperlist")
    Observable<Response<String>> getpaperlist(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/searchpaper")
    Observable<Response<String>> getsearchpaper(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/subject-list")
    Observable<Response<String>> getsubjectlist(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/paperquestionlist")
    Observable<Response<String>> getQuestionlist(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/test_report")
    Observable<Response<String>> getTestreport(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/test-summary")
    Observable<Response<String>> getTestSummary(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("student/submit-test")
    Observable<Response<String>> submitest(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("querymessage")
    Observable<Response<String>> sendfeedback(@FieldMap Map<String, String> stringMap);

    @GET("level")
    Observable<Response<String>> getLevel();

    @GET("legal/student")
    Observable<Response<String>> getLegal();

    @GET("zooki")
    Observable<Response<String>> getZooki();

    @GET("student/profile")
    Observable<Response<String>> getProfile();

    //    @Multipart
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @HTTP(method = "DELETE", path = "peep/", hasBody = true)
    Observable<Response<String>> peepDelete(@Body RequestBody object);

    String MULTIPART_FORM_DATA = "multipart/form-data";

    static RequestBody createRequestBody(@NonNull String s) {
        return RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), s);
    }

}
