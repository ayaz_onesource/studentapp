package com.onesourceweb.student.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.onesourceweb.student.BaseActivity;
import com.onesourceweb.student.R;
import com.onesourceweb.student.adpater.AssginmentAdpater;
import com.onesourceweb.student.adpater.TestReportAdpater;
import com.onesourceweb.student.model.AssginmentModel;
import com.onesourceweb.student.model.TestReportLevelModel;
import com.onesourceweb.student.network.NetworkRequest;
import com.onesourceweb.student.utils.Constant;
import com.onesourceweb.student.utils.L;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Subscription;

public class TestReportActivity extends BaseActivity {

    private static final String TAG = "TestReportActivity";
    private ImageView mImgBackReport;
    private TextView mTvTestSummary;

    RecyclerView mMstudentList;
    TestReportAdpater testReportAdpater;
    private TextView mTestName;
    private TextView mSubjectName;
    private TextView mMTvscore;
    private TextView mMTvcorrect;
    private TextView mMTvincorrect;
    private TextView mMTvnoanswer;
    private ProgressBar mProgSubject;
    private TextView mMTvtakentime;
    private TextView mMTvavgtime;
    private TextView sub_accu;
    Subscription subscription;

    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_report);

        id = getIntent().getStringExtra(Constant.PAPERID);

        initView();

        if (L.isNetworkAvailable(this)) {
            callTestReport();
        }


        mMstudentList.setLayoutManager(new GridLayoutManager(TestReportActivity.this, 3));


        mImgBackReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(TestReportActivity.this, TestActivity.class));
//                finish();
                onBackPressed();
            }
        });
        mTvTestSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TestReportActivity.this, TestSummaryActivity.class);
                i.putExtra(Constant.PAPERID, id);
                startActivity(i);
                // finish();
            }
        });
    }

    private void initView() {
        mImgBackReport = (ImageView) findViewById(R.id.img_back_report);
        mTvTestSummary = (TextView) findViewById(R.id.tv_test_summary);
        mMstudentList = (RecyclerView) findViewById(R.id.mstudentList);

        mTestName = (TextView) findViewById(R.id.test_name);
        mSubjectName = (TextView) findViewById(R.id.subject_name);
        mMTvscore = (TextView) findViewById(R.id.mTvscore);
        mMTvcorrect = (TextView) findViewById(R.id.mTvcorrect);
        mMTvincorrect = (TextView) findViewById(R.id.mTvincorrect);
        mMTvnoanswer = (TextView) findViewById(R.id.mTvnoanswer);
        mProgSubject = (ProgressBar) findViewById(R.id.prog_subject);
        mMTvtakentime = (TextView) findViewById(R.id.mTvtakentime);
        mMTvavgtime = (TextView) findViewById(R.id.mTvavgtime);
        sub_accu = (TextView) findViewById(R.id.sub_accu);
    }

    private void callTestReport() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.PAPERID, id);

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getTestreport(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    Log.d(TAG, "testreport " + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject data1 = jsonResponse.getJSONObject("data");
                    JSONObject testDetails = data1.getJSONObject("test_details");

                    int total = testDetails.getInt("TotalQuestion");

                    mSubjectName.setText(testDetails.getString("SubjectName"));
                    mMTvscore.setText(testDetails.getString("TotalRight") + "-" + total);
                    mMTvcorrect.setText(testDetails.getString("TotalRight") + "-" + total);
                    mMTvincorrect.setText(testDetails.getString("TotalWrong") + "-" + total);
                    mMTvnoanswer.setText((total-testDetails.getInt("TotalAttempt")) + "-" + total);
                    mProgSubject.setProgress(Integer.parseInt(testDetails.getString("subjectaccuracy")));
                    sub_accu.setText("Your Accuracy for All Subject : " + (testDetails.getString("subjectaccuracy")) + " %");
                    mMTvtakentime.setText(testDetails.getString("TakenTime"));
                    mMTvavgtime.setText(testDetails.getString("AvgTime"));

                    JSONArray level = data1.getJSONArray("level_questions");
                    List<TestReportLevelModel> testReportLevelModels = new ArrayList<>();

                    for (int i = 0; i < level.length(); i++) {

                        JSONObject lv = level.getJSONObject(i);

                        TestReportLevelModel testReportLevelModel = new TestReportLevelModel(lv.getString("Level"), lv.getInt("Count"),total);
                        testReportLevelModels.add(testReportLevelModel);

                    }

                    testReportAdpater = new TestReportAdpater(this, testReportLevelModels);
                    mMstudentList.setAdapter(testReportAdpater);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }
}
