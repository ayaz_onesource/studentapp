package com.onesourceweb.student.adpater;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.onesourceweb.student.fragment.ZookiFragment;
import com.onesourceweb.student.model.ZookiIteamModel;

import java.util.List;

public class ZookiPagerAdpater extends FragmentStatePagerAdapter {

    private static final String TAG = "vxcv";
    List<ZookiIteamModel> zookiIteamModels;


    public ZookiPagerAdpater(FragmentManager fragmentManager, List<ZookiIteamModel> zookiIteamModels) {
        super(fragmentManager);
        this.zookiIteamModels = zookiIteamModels;
    }

    @Override
    public int getCount() {
        return zookiIteamModels.size();

    }

    @Override
    public Fragment getItem(int position) {
        return ZookiFragment.newInstance(zookiIteamModels.get(position));
    }

}