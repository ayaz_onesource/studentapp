package com.onesourceweb.student.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.onesourceweb.student.BaseActivity;
import com.onesourceweb.student.R;
import com.onesourceweb.student.adpater.LegalListviewAdapater;
import com.onesourceweb.student.model.LegalDataModel;
import com.onesourceweb.student.network.NetworkRequest;
import com.onesourceweb.student.utils.L;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Subscription;

public class LegalActivity extends BaseActivity {

    List<LegalDataModel> legalDataModels;
    ListView mList;
    private ImageView mImgBackReport;
    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal);

        mList = findViewById(R.id.mList);
        legalDataModels = new ArrayList<>();

//        legalDataModels.add(new LegalDataModel("Privacy policy", "fgjdsjgjdsgfdvb budfhgbhdfuhg bvdjhfgbhfdgbhfdhgbd bjhdfgbjdfgbjd jbgjhdfg"));
//        legalDataModels.add(new LegalDataModel("Term & Condition", "fgjdsjgjdsgfdvb budfhgbhdfuhg bvdjhfgbhfdgbhfdhgbd bjhdfgbjdfgbjd jbgjhdfg"));
//        legalDataModels.add(new LegalDataModel("Refund & Cacellation", "fgjdsjgjdsgfdvb budfhgbhdfuhg bvdjhfgbhfdgbhfdhgbd bjhdfgbjdfgbjd jbgjhdfg"));
//        legalDataModels.add(new LegalDataModel("Licenses & Copyright", "fgjdsjgjdsgfdvb budfhgbhdfuhg bvdjhfgbhfdgbhfdhgbd bjhdfgbjdfgbjd jbgjhdfg"));

        //listAdapter = new LegalListAdpater(LegalActivity.this, legalDataModels);
        //expListView.setAdapter(listAdapter);

//        mstudentList.setHasFixedSize(true);
//        mstudentList.setItemAnimator(new DefaultItemAnimator());
//        mstudentList.setLayoutManager(new LinearLayoutManager(LegalActivity.this));


        initView();

        if (L.isNetworkAvailable(LegalActivity.this)) {
            callLegal();
        }

        mImgBackReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LegalActivity.this, MainActivity.class));
                finish();
            }
        });
    }

    private void callLegal() {
        Map<String, String> map = new HashMap<>();
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLegal(), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    legalDataModels.addAll(LoganSquare.parseList(jsonResponse.getJSONArray("data").toString(), LegalDataModel.class));

                    mList.setAdapter(new LegalListviewAdapater(LegalActivity.this, legalDataModels));

//                    JSONArray legalData = jsonResponse.getJSONArray("data");
//
//                    for (int i = 0; i < legalData.length(); i++) {
//
//                        JSONObject le = legalData.getJSONObject(i);
//                        LegalDataModel legalDataModel = new LegalDataModel(le.getString("Name"), le.getString("value"));
//                        legalDataModels.add(legalDataModel);
//                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }

    private void initView() {

        mImgBackReport = (ImageView) findViewById(R.id.img_back_report);
    }
}
