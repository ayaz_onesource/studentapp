package com.onesourceweb.student.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.onesourceweb.student.BaseActivity;
import com.onesourceweb.student.R;
import com.onesourceweb.student.fragment.MenuBottomSheetFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.img_menu)
    ImageView imgMenu;
    @BindView(R.id.imglogo)
    ImageView imglogo;
    @BindView(R.id.massignment)
    LinearLayout massignment;
    @BindView(R.id.msearchpaper)
    LinearLayout msearchpaper;
    @BindView(R.id.mreport)
    LinearLayout mreport;
    @BindView(R.id.mzooki)
    LinearLayout mzooki;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("You Want To Exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.this.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    @OnClick({R.id.img_menu, R.id.massignment, R.id.msearchpaper, R.id.mreport, R.id.mzooki})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_menu:
                MenuBottomSheetFragment bottomSheetFragment = new MenuBottomSheetFragment();
                bottomSheetFragment.setCancelable(false);
                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                break;
            case R.id.massignment:
                startActivity(new Intent(MainActivity.this, Assginment.class));
                break;
            case R.id.msearchpaper:
                startActivity(new Intent(MainActivity.this, SearchTestPapers.class));
                break;
            case R.id.mreport:
                break;
            case R.id.mzooki:
                startActivity(new Intent(MainActivity.this, ZookiActivity.class));
                break;
        }
    }
}
