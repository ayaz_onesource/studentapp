package com.onesourceweb.student.adpater;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onesourceweb.student.R;
import com.onesourceweb.student.activity.SearchTestPapers;
import com.onesourceweb.student.model.FilterLevelListModel;
import com.onesourceweb.student.model.FilterSubjectListModel;
import com.onesourceweb.student.model.SummaryOptionModel;

import java.util.List;

public class FilterListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;

    List<FilterSubjectListModel> filterSubjectListModels;


    public FilterListAdpater(Context context, List<FilterSubjectListModel> filterSubjectListModels) {
        this.context = context;
        this.filterSubjectListModels = filterSubjectListModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_list_row_iteam, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderIn, final int position) {

        final ViewHolder holder = (ViewHolder) holderIn;

        FilterSubjectListModel filterSubjectListModel = filterSubjectListModels.get(position);

        //Log.d(TAG, "SummaryCurrentModel data: " + summaryOptionModel.getOptionText());

        if (position == 0)
            holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.firstpos));
        else if (position == getItemCount() - 1)
            holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.lastpos));
        else
            holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.defult_pos));

        holder.tvfiltersub.setText(filterSubjectListModel.getSubjectname());
//        if (filterSubjectListModel.getCheck() == 1) {
//            holder.chkfilter.setChecked(true);
//
//        } else {
//            holder.chkfilter.setChecked(false);
//
//        }


    }


    @Override
    public int getItemCount() {
        return filterSubjectListModels.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvfiltersub;
        private LinearLayout rowmainlayout;
        private CheckBox chkfilter;


        public ViewHolder(View v) {
            super(v);
            tvfiltersub = (TextView) v.findViewById(R.id.tvfiltersub);
            chkfilter = (CheckBox) v.findViewById(R.id.chkfilter);
            rowmainlayout = (LinearLayout) v.findViewById(R.id.filterrowmainlayout);

        }

    }

}
