package com.onesourceweb.student.model;

public class TestReportLevelModel {

    String Level;
    int Count;
    int total;

    public String getLevel() {
        return Level;
    }

    public void setLevel(String level) {
        Level = level;
    }

    public int getCount() {
        return Count;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setCount(int count) {
        Count = count;
    }

    public TestReportLevelModel(String level, int count, int total) {
        Level = level;
        Count = count;
        this.total = total;
    }
}
