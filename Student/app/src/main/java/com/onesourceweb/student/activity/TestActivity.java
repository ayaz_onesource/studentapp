package com.onesourceweb.student.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.onesourceweb.student.BaseActivity;
import com.onesourceweb.student.R;
import com.onesourceweb.student.adpater.TestPagerAdapterNew;
import com.onesourceweb.student.model.SubmitTest;
import com.onesourceweb.student.model.TestModel;
import com.onesourceweb.student.model.TestOptionListModel;
import com.onesourceweb.student.network.NetworkRequest;
import com.onesourceweb.student.utils.Constant;
import com.onesourceweb.student.utils.L;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

import static android.content.ContentValues.TAG;

public class TestActivity extends BaseActivity {


    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.btn_endtest)
    Button btnEndtest;
    @BindView(R.id.la)
    LinearLayout la;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.img_qustion_back)
    ImageView imgQustionBack;
    @BindView(R.id.tvcount)
    TextView tvcount;
    @BindView(R.id.img_qustion_forward)
    ImageView imgQustionForward;
    @BindView(R.id.l2)
    LinearLayout l2;
    List<TestModel> testModels;

    int qsize;
    int id;
    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);
        id = getIntent().getIntExtra(Constant.MCQPlannerID, 0);
        testModels = new ArrayList<>();
        if (L.isNetworkAvailable(TestActivity.this))
            callQuestion();


        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == (viewpager.getAdapter().getCount() - 1)) {
                    imgQustionForward.setBackgroundResource(R.drawable.rounded_corner_bg_chng);

                } else {
                    tvcount.setText((viewpager.getCurrentItem() - 1) + "/ " + qsize);
                    imgQustionForward.setBackgroundResource(R.drawable.rounded_corner_bg);
                }

                if (position == 0) {
                    imgQustionBack.setBackgroundResource(R.drawable.rounded_corner_bg_chng);
                    tvcount.setText("1" + "/ " + testModels.size());

                } else {
                    tvcount.setText((viewpager.getCurrentItem() + 1) + "/ " + qsize);
                    imgQustionBack.setBackgroundResource(R.drawable.rounded_corner_bg);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void submitTest() {
        List<SubmitTest> submitTests = new ArrayList<>();

        for (int i = 0; i < testModels.size(); i++) {
            TestOptionListModel testOptionListModel = null;
            for (int j = 0; j < testModels.get(i).getOption().size(); j++) {
                if (testModels.get(i).getOption().get(j).getIsslected()) {
                    testOptionListModel = testModels.get(i).getOption().get(j);
                    break;
                }
            }
            if (testOptionListModel != null) {
                submitTests.add(new SubmitTest(testOptionListModel.getMCQOptionID(), "1", testModels.get(i).getMCQPlannerDetailsID()));
            } else {
                submitTests.add(new SubmitTest(0, "0", testModels.get(i).getMCQPlannerDetailsID()));
            }
        }

        Log.d(TAG, "submitTest: " + new Gson().toJson(submitTests));
        submitTestdata(new Gson().toJson(submitTests));
    }

    private void callQuestion() {
        Map<String, String> map = new HashMap<>();
        map.put("PaperID", String.valueOf(id));
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getQuestionlist(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    Log.d(TAG, "QuestionList" + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONArray DATA = jsonResponse.getJSONArray("data");

                    for (int i = 0; i < DATA.length(); i++) {
                        JSONObject O = DATA.getJSONObject(i);
                        JSONArray option = O.getJSONArray("Options");

                        List<TestOptionListModel> optionlist = new ArrayList<>();
                        for (int j = 0; j < option.length(); j++) {
                            JSONObject op = option.getJSONObject(j);
                            TestOptionListModel testOptionListModel = new TestOptionListModel(op.getInt("MCQOptionID"), "A", op.getString("Options"));
                            optionlist.add(testOptionListModel);
                        }

                        // String Question = O.getString("Question");

                        TestModel testModel = new TestModel(O.getInt("MCQPlannerDetailsID"), O.getString("Question"), optionlist);
                        testModels.add(testModel);
                    }


                    viewpager.setAdapter(new TestPagerAdapterNew(getSupportFragmentManager(), testModels));

                    qsize = testModels.size();
                    tvcount.setText((viewpager.getCurrentItem() + 1) + "/ " + qsize);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }

    private void submitTestdata(String detailsArray) {
        Map<String, String> map = new HashMap<>();
        map.put("PaperID", String.valueOf(id));
        map.put("detail_array", detailsArray);
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.submitest(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    Log.d(TAG, "testresp " + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject("data");
                    startActivity(new Intent(TestActivity.this, TestReportActivity.class).putExtra(Constant.PAPERID, DATA.getString("StudentMCQTestID")));
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }


    public void onBackPressed() {
        super.onBackPressed();

    }

    @OnClick({R.id.img_back, R.id.img_qustion_back, R.id.img_qustion_forward, R.id.btn_endtest})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_endtest:
                submitTest();
                break;
            case R.id.img_qustion_back:
                viewpager.setCurrentItem(viewpager.getCurrentItem() - 1, true);
                break;
            case R.id.img_qustion_forward:
                viewpager.setCurrentItem(viewpager.getCurrentItem() + 1, true);

                break;
        }
    }
}
