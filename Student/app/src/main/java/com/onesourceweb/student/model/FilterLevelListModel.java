package com.onesourceweb.student.model;

public class FilterLevelListModel {

    String LevelID, LevelName;

    public String getLevelID() {
        return LevelID;
    }

    public void setLevelID(String levelID) {
        LevelID = levelID;
    }

    public String getLevelName() {
        return LevelName;
    }

    public void setLevelName(String levelName) {
        LevelName = levelName;
    }

    public FilterLevelListModel(String levelID, String levelName) {
        LevelID = levelID;
        LevelName = levelName;
    }
}
