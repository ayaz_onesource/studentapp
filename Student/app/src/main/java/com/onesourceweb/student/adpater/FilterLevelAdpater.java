package com.onesourceweb.student.adpater;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.onesourceweb.student.R;
import com.onesourceweb.student.model.FilterLevelListModel;
import com.onesourceweb.student.model.FilterSubjectListModel;

import java.util.List;

public class FilterLevelAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;

    List<FilterLevelListModel> filterLevelListModels;


    public FilterLevelAdpater(Context context, List<FilterLevelListModel> filterLevelListModels) {
        this.context = context;
        this.filterLevelListModels = filterLevelListModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_list_row_iteam, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderIn, final int position) {

        final ViewHolder holder = (ViewHolder) holderIn;

        final FilterLevelListModel filterLevelListModel = filterLevelListModels.get(position);

        //Log.d(TAG, "SummaryCurrentModel data: " + summaryOptionModel.getOptionText());

        if (position == 0)
            holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.firstpos));
        else if (position == getItemCount() - 1)
            holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.lastpos));
        else
            holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.defult_pos));

        holder.tvfiltersub.setText(filterLevelListModel.getLevelName());
//        if (filterLevelListModel.getCheck() == 1) {
//            holder.chkfilter.setChecked(true);
//
//        } else {
//            holder.chkfilter.setChecked(false);
//
//        }

//        holder.rowmainlayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String a = filterLevelListModel.getLevelID();
//                Toast.makeText(context, a, Toast.LENGTH_SHORT).show();
//            }
//        });


        holder.chkfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a = filterLevelListModel.getLevelID();
                Toast.makeText(context, a, Toast.LENGTH_SHORT).show();


            }
        });


    }


    @Override
    public int getItemCount() {
        return filterLevelListModels.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvfiltersub;
        private LinearLayout rowmainlayout;
        private CheckBox chkfilter;


        public ViewHolder(View v) {
            super(v);
            tvfiltersub = (TextView) v.findViewById(R.id.tvfiltersub);
            chkfilter = (CheckBox) v.findViewById(R.id.chkfilter);
            rowmainlayout = (LinearLayout) v.findViewById(R.id.filterrowmainlayout);

        }

    }

}
