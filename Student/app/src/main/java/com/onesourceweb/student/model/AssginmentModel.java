package com.onesourceweb.student.model;


import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class AssginmentModel {

    @JsonField
    int MCQPlannerID;
    @JsonField
    int TotalQuestion;
    @JsonField
    String SubjectName;
    @JsonField
    String StandardName;
    @JsonField
    String MediumName;
    @JsonField
    String BoardName;
    @JsonField
    String BranchName;
    @JsonField
    int BranchID;
    @JsonField
    String StartDate;
    @JsonField
    String EndDate;

    public int getMCQPlannerID() {
        return MCQPlannerID;
    }

    public void setMCQPlannerID(int MCQPlannerID) {
        this.MCQPlannerID = MCQPlannerID;
    }

    public int getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public int getBranchID() {
        return BranchID;
    }

    public void setBranchID(int branchID) {
        BranchID = branchID;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }
}
