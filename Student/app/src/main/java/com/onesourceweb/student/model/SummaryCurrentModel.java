package com.onesourceweb.student.model;

import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class SummaryCurrentModel implements Serializable {


    String question;
    int questionindex;
    String AnswerID;
    List<SummaryOptionModel> option;

    public String getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(String answerID) {
        AnswerID = answerID;
    }

    public SummaryCurrentModel(String question, int questionindex, String answerID, List<SummaryOptionModel> option) {
        this.question = question;
        this.questionindex = questionindex;
        AnswerID = answerID;
        this.option = option;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }


    public List<SummaryOptionModel> getOption() {
        return option;
    }

    public void setOption(List<SummaryOptionModel> option) {
        this.option = option;
    }

    public int getQuestionindex() {
        return questionindex;
    }

    public void setQuestionindex(int questionindex) {
        this.questionindex = questionindex;
    }
}
