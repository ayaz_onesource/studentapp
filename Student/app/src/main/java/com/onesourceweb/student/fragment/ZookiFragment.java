package com.onesourceweb.student.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.onesourceweb.student.R;
import com.onesourceweb.student.model.ZookiIteamModel;
import com.onesourceweb.student.utils.L;
import com.onesourceweb.student.utils.Prefs;
import com.squareup.picasso.Picasso;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.ContentValues.TAG;

public class ZookiFragment extends Fragment {

    public ZookiIteamModel zookiIteamModel;

    @BindView(R.id.img_zooki)
    private ImageView mImgZooki;

    private ProgressBar img_prog;
    private TextView mZookiDate, mContaint, mZookiSubtittle, mtiitle;
    Context context;

    public static Fragment newInstance(ZookiIteamModel zookiIteamModel) {

        ZookiFragment testFragment = new ZookiFragment();
        Bundle args = new Bundle();
        args.putSerializable("testModel", (Serializable) zookiIteamModel);
        testFragment.setArguments(args);
        Log.d(TAG, "zooki bundle data: " + args.size());

        return testFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.zooki_pager_iteam, container, false);
        ButterKnife.bind(this, v);
        initView();
        zookiIteamModel = (ZookiIteamModel) getArguments().getSerializable("testModel");

//        mImgZooki = (ImageView) v.findViewById(R.id.img_zooki);
//        mZookiDate = (TextView) v.findViewById(R.id.zooki_date);
//        mZookiSubtittle = (TextView) v.findViewById(R.id.zooki_subtittle);
//        mContaint = (TextView) v.findViewById(R.id.containt);
//        mtiitle = (TextView) v.findViewById(R.id.zooki_tittle);
//        img_prog = (ProgressBar) v.findViewById(R.id.img_prog);


        mtiitle.setText(zookiIteamModel.getTitle());
        mZookiSubtittle.setText(zookiIteamModel.getTitle());
        mZookiDate.setText(zookiIteamModel.getCreatedDate());
        mContaint.setText(zookiIteamModel.getDescription());
        L.loadImageWithPicasso(getContext(), zookiIteamModel.getImage(), mImgZooki, img_prog);

        return v;
    }

    private void initView() {

    }
}
