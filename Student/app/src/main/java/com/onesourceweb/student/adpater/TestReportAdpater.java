package com.onesourceweb.student.adpater;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.onesourceweb.student.List_Onclick;
import com.onesourceweb.student.R;
import com.onesourceweb.student.activity.Assginment;
import com.onesourceweb.student.activity.TestReportActivity;
import com.onesourceweb.student.model.AssginmentModel;
import com.onesourceweb.student.model.TestOptionListModel;
import com.onesourceweb.student.model.TestReportLevelModel;

import java.util.List;

public class TestReportAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<TestReportLevelModel> testReportLevelModels;

    public TestReportAdpater(TestReportActivity testReportActivity, List<TestReportLevelModel> testReportLevelModels) {
        this.context = testReportActivity;
        this.testReportLevelModels = testReportLevelModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.testreport_list_iteam, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        TestReportLevelModel testOptionListModel = testReportLevelModels.get(position);
        holder.mTvcount.setText(testOptionListModel.getCount() + " - " + testOptionListModel.getTotal());
        holder.mTvlevel.setText(testOptionListModel.getLevel());
        // holder.mTvcount.setText((testOptionListModel.getCount()));
    }

    @Override
    public int getItemCount() {
        return testReportLevelModels.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar mProglevel;
        private TextView mTvlevel, mTvcount;
        //private LinearLayout rowmainlayout;

        public ViewHolder(View v) {
            super(v);
            mProglevel = (ProgressBar) v.findViewById(R.id.prog_level);
            mTvlevel = (TextView) v.findViewById(R.id.mTvlevel);
            mTvcount = (TextView) v.findViewById(R.id.mTvcount);
            //rowmainlayout = (LinearLayout) v.findViewById(R.id.rowmainlayout);
        }

    }
}
