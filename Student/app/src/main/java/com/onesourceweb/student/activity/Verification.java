package com.onesourceweb.student.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.onesourceweb.student.BaseActivity;
import com.onesourceweb.student.R;
import com.onesourceweb.student.network.NetworkRequest;
import com.onesourceweb.student.utils.Constant;
import com.onesourceweb.student.utils.L;
import com.onesourceweb.student.utils.Validation;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;

public class Verification extends LoginActivity {


    private static final String TAG = "Verification";
    private ImageView mImgBack;
    private ImageView mImglogo;
    private TextView mTxtStudentid;
    private MaterialEditText mEdtVerify;
    private Button mBtnVerify;
    public TextView mTvResend;
    Subscription subscription;
    String studentcode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification_student);

        String otp = getIntent().getStringExtra(Constant.Otp);
        String mobileno = getIntent().getStringExtra(Constant.MobileNo);
        studentcode = getIntent().getStringExtra(Constant.StudentCode);

        initView();

        mEdtVerify.setText(otp);

        mTvResend.setPaintFlags(mTvResend.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        mTvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callresendotp();
            }
        });
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Verification.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        mBtnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Validation.isEmpty(mEdtVerify.getText().toString().trim())) {
                    mEdtVerify.setError("Please enter Student Id");
                } else {
                    if (L.isNetworkAvailable(Verification.this))
                        callverify();
                }
            }
        });
    }

    private void callverify() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.StudentCode, studentcode);
        map.put(Constant.Otp, mEdtVerify.getText().toString().trim());
        map.put(Constant.DeviceID, L.getDeviceId(this));
        map.put(Constant.PlayerID, "sadsa");

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getverification(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    Log.d(TAG, "Verifaction data" + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject("data");
                    // String otp = phone.getString(Constant.LoginToken);
                    prefs.save(Constant.loginAuthToken, DATA.getString("login_token"));
                    JSONObject student = DATA.getJSONObject("student");

                    prefs.save(Constant.USERID, student.getString("id"));
                    prefs.save(Constant.isLogin, true);

                    Toast.makeText(this, DATA.getString("login_token") + "  " + student.getString("id"), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(this, MainActivity.class);
                    // intent.putExtra(Constant.MobileNo, mobileno);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });

    }

    public void callresendotp() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.StudentCode, studentcode);
        map.put(Constant.DeviceID, L.getDeviceId(this));
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLogin(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    // Log.d(TAG, "callresend ==: " + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject phone = jsonResponse.getJSONObject("data");
                    String otp = phone.getString("otp");
                    mEdtVerify.setText(otp);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }

    public void initView() {
        mImgBack = (ImageView) findViewById(R.id.img_back);
        mImglogo = (ImageView) findViewById(R.id.imglogo);
        mTxtStudentid = (TextView) findViewById(R.id.txt_studentid);
        mEdtVerify = (MaterialEditText) findViewById(R.id.edt_verify);
        mBtnVerify = (Button) findViewById(R.id.btn_verify);
        mTvResend = (TextView) findViewById(R.id.tv_resend);
    }
}
