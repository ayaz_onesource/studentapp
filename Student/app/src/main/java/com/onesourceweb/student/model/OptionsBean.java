package com.onesourceweb.student.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class OptionsBean implements Serializable {
    /*
    "MCQOptionID": "4",
                "Options": "bbbb<\/p>\r\n",
                "IsCorrect": "1"
     */
    @JsonField
    int MCQOptionID;
    @JsonField
    String Options;
    @JsonField
    String IsCorrect;

    public int getMCQOptionID() {
        return MCQOptionID;
    }

    public void setMCQOptionID(int MCQOptionID) {
        this.MCQOptionID = MCQOptionID;
    }

    public String getOptions() {
        return Options;
    }

    public void setOptions(String options) {
        Options = options;
    }

    public String getIsCorrect() {
        return IsCorrect;
    }

    public void setIsCorrect(String isCorrect) {
        IsCorrect = isCorrect;
    }
}
