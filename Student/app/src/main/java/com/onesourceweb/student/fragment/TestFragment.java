package com.onesourceweb.student.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.onesourceweb.student.QuestionsWebView;
import com.onesourceweb.student.R;
import com.onesourceweb.student.adpater.TestAdpater;
import com.onesourceweb.student.model.TestModel;

import java.io.Serializable;

import static android.content.ContentValues.TAG;

public class TestFragment extends Fragment {

    public TestModel testModelData;

    public static Fragment newInstance(TestModel testModel) {

        TestFragment testFragment = new TestFragment();
        Bundle args = new Bundle();
        args.putSerializable("testModel", (Serializable) testModel);
        testFragment.setArguments(args);
        Log.d(TAG, "bundle data: " + args.size());

        return testFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layoutView = inflater.inflate(R.layout.question_iteam, container, false);
//        TestModel testModels = (TestModel) getArguments().getSerializable("testModels");
        testModelData = (TestModel) getArguments().getSerializable("testModel");
        Log.d(TAG, "size: " + testModelData);


        RecyclerView mMstudentList = (RecyclerView) layoutView.findViewById(R.id.mstudentList);


        QuestionsWebView question = (QuestionsWebView) layoutView.findViewById(R.id.question);

        mMstudentList.setHasFixedSize(true);
        mMstudentList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mMstudentList.setItemAnimator(new DefaultItemAnimator());

        question.getSettings().setJavaScriptEnabled(true);
        //question.loadData(testModelData.getQuestion(), "text/html", "UTF-8");

        question.loadHtmlFromLocal(testModelData.getQuestion());
        //  question.setInitialScale(40);

        // question.setBackgroundColor(Color.TRANSPARENT);
       // question.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
       // question.setScrollbarFadingEnabled(false);
       // question.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        mMstudentList.setAdapter(new TestAdpater(getActivity(), testModelData.getOption(), testModelData));


        return layoutView;
    }

}
