package com.onesourceweb.student.model;

import java.io.Serializable;

public class ZookiIteamModel implements Serializable {

    String ZookiID, Title, Description, Image, CreatedDate, ModifiedDate;

    public String getZookiID() {
        return ZookiID;
    }

    public void setZookiID(String zookiID) {
        ZookiID = zookiID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public ZookiIteamModel(String zookiID, String title, String description, String image, String createdDate, String modifiedDate) {
        ZookiID = zookiID;
        Title = title;
        Description = description;
        Image = image;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
    }
}
