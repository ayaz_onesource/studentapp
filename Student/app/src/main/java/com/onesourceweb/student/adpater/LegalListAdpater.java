package com.onesourceweb.student.adpater;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onesourceweb.student.R;
import com.onesourceweb.student.model.LegalDataModel;

import java.util.List;

public class LegalListAdpater extends BaseExpandableListAdapter {

    private Context context;
    String childText;

    List<LegalDataModel> legalDataModels;

    public LegalListAdpater(Context context, List<LegalDataModel> legalDataModels) {
        this.context = context;
        this.legalDataModels = legalDataModels;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        //   return this.legalDataModels.get(this.legalDataModels.get(groupPosition)).get(childPosititon);
        return childPosititon;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        //childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.legel_list_child_iteam, null);
        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.txtlistcontain);

        txtListChild.setText(legalDataModels.get(groupPosition).getSubdata());
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
//        return this._listDataChild.get(this._listDataHeader.get(groupPosition)) .size();
        return groupPosition;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.legalDataModels.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return legalDataModels.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int Position, boolean isExpanded, View convertView, ViewGroup parent) {

        LegalDataModel dataModel = legalDataModels.get(Position);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.title);
       // TextView txtlistcontain = (TextView) convertView.findViewById(R.id.txtsubdata);
        LinearLayout rowmainlayout = (LinearLayout) convertView.findViewById(R.id.legelrow);

//            if (Position == 0)
//                rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.firstpos));
//            else if (Position == get- 1)
//                rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.lastpos));
//            else
//                rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.defult_pos));

        lblListHeader.setText(dataModel.getTittle());
        //txtlistcontain.setText(dataModel.getSubdata());

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}