package com.onesourceweb.student.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onesourceweb.student.R;
import com.onesourceweb.student.activity.LegalActivity;
import com.onesourceweb.student.activity.ProfileActivity;
import com.onesourceweb.student.activity.QueryActivity;
import com.onesourceweb.student.utils.L;
import com.onesourceweb.student.utils.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.Subscription;

public class MenuBottomSheetFragment extends BottomSheetDialogFragment {

    private static final String TAG = "BottomSheetFragment";

    public Prefs prefs;
    Context context;
    Subscription subscription;
    @BindView(R.id.img_close)
    ImageView imgClose;
    @BindView(R.id.mTvdashboard)
    TextView mTvdashboard;
    @BindView(R.id.mTvprofile)
    TextView mTvprofile;
    @BindView(R.id.mTvquery)
    TextView mTvquery;
    @BindView(R.id.mTvlegal)
    TextView mTvlegal;
    @BindView(R.id.mTvlogout)
    TextView mTvlogout;
    Unbinder unbinder;

    public MenuBottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.menubottomsheet, container, false);

        prefs = Prefs.with(context);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.img_close, R.id.mTvdashboard, R.id.mTvprofile, R.id.mTvquery, R.id.mTvlegal, R.id.mTvlogout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_close:
                onDismiss(getDialog());
                break;
            case R.id.mTvdashboard:
                onDismiss(getDialog());
                break;
            case R.id.mTvprofile:
                onDismiss(getDialog());
                startActivity(new Intent(getActivity(), ProfileActivity.class));
                break;
            case R.id.mTvquery:
                onDismiss(getDialog());
                startActivity(new Intent(getActivity(), QueryActivity.class));
                break;
            case R.id.mTvlegal:
                onDismiss(getDialog());
                startActivity(new Intent(getActivity(), LegalActivity.class));
                break;
            case R.id.mTvlogout:
                L.logout(getActivity());
                break;
        }
    }
}

