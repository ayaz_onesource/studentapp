package com.onesourceweb.student.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.onesourceweb.student.R;
import com.onesourceweb.student.adpater.FilterLevelAdpater;
import com.onesourceweb.student.adpater.FilterListAdpater;
import com.onesourceweb.student.model.FilterLevelListModel;
import com.onesourceweb.student.model.FilterSubjectListModel;
import com.onesourceweb.student.network.NetworkRequest;
import com.onesourceweb.student.network.RestAPIBuilder;
import com.onesourceweb.student.network.RestApi;
import com.onesourceweb.student.utils.L;
import com.onesourceweb.student.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import rx.Subscription;

public class BottomSheetFragment extends BottomSheetDialogFragment {

    private static final String TAG = "BottomSheetFragment";

    private Button mBtnSubject, mBtnLevel, mBtnReset, mBtnSave;
    private RecyclerView mMstudentList;
    Context context;

    public Prefs prefs;

    protected RestApi restApi;
    Subscription subscription;

    ArrayList<FilterSubjectListModel> filterSubjectListModels;
    ArrayList<FilterLevelListModel> filterLevelListModels;

    public BottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.filteractivity, container, false);

        restApi = RestAPIBuilder.buildRetrofitService();
        prefs = Prefs.with(context);

        filterSubjectListModels = new ArrayList<>();
        filterLevelListModels = new ArrayList<>();
//        filterSubjectListModels.add(new FilterSubjectListModel("Hindi", 0));
//        filterSubjectListModels.add(new FilterSubjectListModel("English", 1));
//        filterSubjectListModels.add(new FilterSubjectListModel("Maths", 1));
//        filterSubjectListModels.add(new FilterSubjectListModel("Computer", 0));
//        filterSubjectListModels.add(new FilterSubjectListModel("Android", 0));
//        filterSubjectListModels.add(new FilterSubjectListModel("Java", 0));
//        filterSubjectListModels.add(new FilterSubjectListModel("Php", 0));
//        filterSubjectListModels.add(new FilterSubjectListModel("Iphone", 0));
//
//        filterLevelListModels.add(new FilterLevelListModel("Level 1", 1));
//        filterLevelListModels.add(new FilterLevelListModel("Level 2", 1));
//        filterLevelListModels.add(new FilterLevelListModel("Level 3", 0));
//        filterLevelListModels.add(new FilterLevelListModel("Level 4", 0));
//        filterLevelListModels.add(new FilterLevelListModel("Level 5", 0));


        final BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();

        mBtnSubject = (Button) v.findViewById(R.id.btn_subject);
        mBtnLevel = (Button) v.findViewById(R.id.btn_level);
        mBtnReset = (Button) v.findViewById(R.id.btn_reset);
        mBtnSave = (Button) v.findViewById(R.id.btn_save);

        mMstudentList = (RecyclerView) v.findViewById(R.id.mstudentList);


        mMstudentList.setHasFixedSize(true);
        mMstudentList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mMstudentList.setItemAnimator(new DefaultItemAnimator());

        callsubjectlist();

        mBtnSubject.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                mBtnSubject.setBackgroundResource(R.drawable.sub_select);
                mBtnLevel.setBackgroundResource(R.drawable.unselect_level);
                mBtnLevel.setTextColor(Color.parseColor("#DD4C4C"));
                mBtnSubject.setTextColor(Color.parseColor("#ffffff"));

                mMstudentList.setAdapter(new FilterListAdpater(getActivity(), filterSubjectListModels));
            }
        });
        mBtnLevel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                mBtnSubject.setBackgroundResource(R.drawable.unselect);
                mBtnLevel.setBackgroundResource(R.drawable.select);
                mBtnSubject.setTextColor(Color.parseColor("#DD4C4C"));
                mBtnLevel.setTextColor(Color.parseColor("#ffffff"));
                if (filterLevelListModels.isEmpty()) {
                    callLevellist();
                } else {
                    mMstudentList.setAdapter(new FilterLevelAdpater(getActivity(), filterLevelListModels));
                }

            }
        });

        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (behavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
//                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                } else {
//                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                }
                filterLevelListModels.clear();
            }
        });
        mBtnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //filterLevelListModels.clear();
            }
        });

        return v;

    }

    private void callsubjectlist() {
        Map<String, String> map = new HashMap<>();
        // showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getsubjectlist(map), (data) -> {
            //showProgress(false);
            if (data.code() == 200) {
                try {
                    Log.d(TAG, "subjectlist " + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONArray DATA = jsonResponse.getJSONArray("data");
                    for (int i = 0; i < DATA.length(); i++) {
                        JSONObject O = DATA.getJSONObject(i);

                        String SubjectName = O.getString("SubjectName");
                        String SubjectID = O.getString("SubjectID");

                        FilterSubjectListModel assginmentModel = new FilterSubjectListModel(SubjectName, SubjectID);
                        filterSubjectListModels.add(assginmentModel);
                    }

                    mMstudentList.setAdapter(new FilterListAdpater(getActivity(), filterSubjectListModels));


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(context, data);
            }

        }, (e) -> {
            //showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }

    private void callLevellist() {
        // showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLevel(), (data) -> {
            //showProgress(false);
            if (data.code() == 200) {
                try {
                    Log.d(TAG, "subjectlist " + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONArray DATA = jsonResponse.getJSONArray("data");
                    for (int i = 0; i < DATA.length(); i++) {
                        JSONObject O = DATA.getJSONObject(i);

                        String LevelName = O.getString("LevelName");
                        String LevelID = O.getString("LevelID");

                        FilterLevelListModel filterLevelListModel = new FilterLevelListModel(LevelID, LevelName);
                        filterLevelListModels.add(filterLevelListModel);
                    }

                    mMstudentList.setAdapter(new FilterLevelAdpater(getActivity(), filterLevelListModels));


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(context, data);
            }

        }, (e) -> {
            //showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }
}

