package com.onesourceweb.student.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import com.google.gson.Gson;
import com.onesourceweb.student.network.RestAPIBuilder;
import com.onesourceweb.student.network.RestApi;
import com.onesourceweb.student.utils.Prefs;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;

public class BaseFragment extends Fragment {

    protected LinearLayoutManager layoutManager;
    private static final String TAG = "BaseFragment";

    protected RestApi restApi;
    public Prefs prefs;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutManager = new LinearLayoutManager(getActivity());
        restApi = RestAPIBuilder.buildRetrofitService();

        prefs = Prefs.with(getActivity());
    }

    public void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading...");
        }
        if (isShow) {
            if (!progressDialog.isShowing())
                progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

}
