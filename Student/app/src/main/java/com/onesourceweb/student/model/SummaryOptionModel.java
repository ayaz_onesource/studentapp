package com.onesourceweb.student.model;

import java.io.Serializable;

public class SummaryOptionModel implements Serializable {

    String index;
    String optionText;
    String type;

    public String getMCQOptionID() {
        return MCQOptionID;
    }

    public void setMCQOptionID(String MCQOptionID) {
        this.MCQOptionID = MCQOptionID;
    }

    String MCQOptionID;


    public SummaryOptionModel(String index, String optionText, String type, String MCQOptionID) {
        this.index = index;
        this.optionText = optionText;
        this.type = type;
        this.MCQOptionID = MCQOptionID;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
