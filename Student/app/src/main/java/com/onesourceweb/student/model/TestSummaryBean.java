package com.onesourceweb.student.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class TestSummaryBean implements Serializable {

    @JsonField(name = "test_details")
    TestDetails testDetails;

    @JsonField(name = "corret_questions")
    List<QuestionsBean> corretQuestions;

    @JsonField(name = "incorret_questions")
    List<QuestionsBean> incorretQuestions;

    @JsonField(name = "not_appeared_questions")
    List<QuestionsBean> notAppearedQuestions;

    public TestDetails getTestDetails() {
        return testDetails;
    }

    public void setTestDetails(TestDetails testDetails) {
        this.testDetails = testDetails;
    }

    public List<QuestionsBean> getCorretQuestions() {
        return corretQuestions;
    }

    public void setCorretQuestions(List<QuestionsBean> corretQuestions) {
        this.corretQuestions = corretQuestions;
    }

    public List<QuestionsBean> getIncorretQuestions() {
        return incorretQuestions;
    }

    public void setIncorretQuestions(List<QuestionsBean> incorretQuestions) {
        this.incorretQuestions = incorretQuestions;
    }

    public List<QuestionsBean> getNotAppearedQuestions() {
        return notAppearedQuestions;
    }

    public void setNotAppearedQuestions(List<QuestionsBean> notAppearedQuestions) {
        this.notAppearedQuestions = notAppearedQuestions;
    }
}
