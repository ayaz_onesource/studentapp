package com.onesourceweb.student.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TestModel implements Serializable {

    int MCQPlannerDetailsID;
    String question;
    List<TestOptionListModel> option;

    public int getMCQPlannerDetailsID() {
        return MCQPlannerDetailsID;
    }

    public void setMCQPlannerDetailsID(int MCQPlannerDetailsID) {
        this.MCQPlannerDetailsID = MCQPlannerDetailsID;
    }

    public TestModel() {
    }

    public TestModel(int MCQPlannerDetailsID, String question, List<TestOptionListModel> option) {
        this.question = question;
        this.option = option;
        this.MCQPlannerDetailsID = MCQPlannerDetailsID;
    }

    public String getQuestion() {
        return question;
    }

    public List<TestOptionListModel> getOption() {
        return option;
    }
}
