package com.onesourceweb.student.model;

import java.io.Serializable;

public class TestOptionListModel implements Serializable {

    String index;
    String optionText;
    int MCQOptionID;

    public Boolean getIsslected() {
        return isslected;
    }

    public void setIsslected(Boolean isslected) {
        this.isslected = isslected;
    }

    public int getMCQOptionID() {
        return MCQOptionID;
    }

    public void setMCQOptionID(int MCQOptionID) {
        this.MCQOptionID = MCQOptionID;
    }

    Boolean isslected = false;


    public TestOptionListModel() {
    }

    public String getIndex() {
        return index;
    }

    public String getOptionText() {
        return optionText;
    }

    public TestOptionListModel(int MCQOptionID, String index, String optionText) {

        this.MCQOptionID = MCQOptionID;
        this.index = index;
        this.optionText = optionText;
    }
}
