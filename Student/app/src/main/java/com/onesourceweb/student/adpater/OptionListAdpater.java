package com.onesourceweb.student.adpater;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onesourceweb.student.OptionsWebView;
import com.onesourceweb.student.R;
import com.onesourceweb.student.model.OptionsBean;
import com.onesourceweb.student.model.SummaryOptionModel;

import java.util.List;

public class OptionListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;

    List<OptionsBean> summaryOptionModelList;

    private static final String TAG = "OptionListAdpater";
    int answerID = -1;

    public OptionListAdpater(Context context, List<OptionsBean> summaryOptionModels, int answerID) {
        this.context = context;
        this.summaryOptionModelList = summaryOptionModels;
        this.answerID = answerID;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_list_rowiteam, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderIn, final int position) {

        final ViewHolder holder = (ViewHolder) holderIn;
        OptionsBean summaryOptionModel = summaryOptionModelList.get(position);

        holder.mTvqid.setText(position + 1);
        // holder.mTvoption.loadData(summaryOptionModel.getOptions(), "text/html", "UTF-8");

        holder.mTvoption.loadHtmlFromLocal(summaryOptionModel.getOptions());
        holder.mTvoption.setInitialScale(40);

        holder.mTvoption.setBackgroundColor(Color.TRANSPARENT);
        holder.mTvoption.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        holder.mTvoption.setScrollbarFadingEnabled(false);
        holder.mTvoption.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        //holder.mTvoption.setText(Html.fromHtml(summaryOptionModel.getOptionText()));

        if (position == 0) {
            if (summaryOptionModel.getIsCorrect().equalsIgnoreCase("1")) {
                holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.selectfirstrightpos));
                holder.mTvindex.setBackground(ContextCompat.getDrawable(context, R.drawable.select_rounded_corner_bg));
                // holder.mTvoption.setTextColor((Color.WHITE));
                holder.mTvqid.setTextColor(Color.parseColor("#A0DA5E"));
            } else if (summaryOptionModel.getMCQOptionID() == answerID) {
                holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.selectfirstpos));
                holder.mTvindex.setBackground(ContextCompat.getDrawable(context, R.drawable.select_rounded_corner_bg));
                //  holder.mTvoption.setTextColor((Color.WHITE));
                holder.mTvqid.setTextColor(Color.parseColor("#FA605F"));
            } else {
                holder.mTvindex.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_corner_bg2));
                // holder.mTvoption.setTextColor(Color.parseColor("#80000000"));
                holder.mTvqid.setTextColor(Color.parseColor("#80000000"));
                holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.firstpos));
            }

        } else if (position == getItemCount() - 1) {
            if (summaryOptionModel.getIsCorrect().equalsIgnoreCase("1")) {
                holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.selectlastrightpos));
                holder.mTvindex.setBackground(ContextCompat.getDrawable(context, R.drawable.select_rounded_corner_bg));
                //holder.mTvoption.setTextColor((Color.WHITE));
                holder.mTvqid.setTextColor(Color.parseColor("#A0DA5E"));
            } else if (summaryOptionModel.getMCQOptionID()==answerID) {
                holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.selectlastpos));
                holder.mTvindex.setBackground(ContextCompat.getDrawable(context, R.drawable.select_rounded_corner_bg));
                // holder.mTvoption.setTextColor((Color.WHITE));
                holder.mTvqid.setTextColor(Color.parseColor("#FA605F"));
            } else {
                holder.mTvindex.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_corner_bg2));
                // holder.mTvoption.setTextColor(Color.parseColor("#80000000"));
                holder.mTvqid.setTextColor(Color.parseColor("#80000000"));
                holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.lastpos));
            }

        } else {
            if (summaryOptionModel.getIsCorrect().equalsIgnoreCase("1")) {
                holder.rowmainlayout.setBackgroundResource(R.drawable.selectgardiant);
                holder.mTvindex.setBackground(ContextCompat.getDrawable(context, R.drawable.select_rounded_corner_bg));
                //holder.mTvoption.setTextColor((Color.WHITE));
                holder.mTvqid.setTextColor(Color.parseColor("#A0DA5E"));
            } else if (summaryOptionModel.getMCQOptionID()==answerID) {
                holder.rowmainlayout.setBackgroundResource(R.drawable.selectgardiant);
                holder.mTvindex.setBackground(ContextCompat.getDrawable(context, R.drawable.select_rounded_corner_bg));
                //holder.mTvoption.setTextColor((Color.WHITE));
                holder.mTvqid.setTextColor(Color.parseColor("#A0DA5E"));
            } else {
                holder.mTvindex.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_corner_bg2));
                // holder.mTvoption.setTextColor(Color.parseColor("#80000000"));
                holder.mTvqid.setTextColor(Color.parseColor("#80000000"));
                holder.rowmainlayout.setBackground(ContextCompat.getDrawable(context, R.drawable.defult_pos));
            }
        }

    }


    @Override
    public int getItemCount() {
        return summaryOptionModelList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvqid;
        OptionsWebView mTvoption;
        private LinearLayout rowmainlayout, mTvindex;

        public ViewHolder(View v) {
            super(v);
            mTvqid = (TextView) v.findViewById(R.id.tv_qid_summary);
            mTvoption = (OptionsWebView) v.findViewById(R.id.tv_option_summary);
            mTvindex = (LinearLayout) v.findViewById(R.id.summary_index_layout);
            rowmainlayout = (LinearLayout) v.findViewById(R.id.rowmainlayout1);

        }

    }

}
