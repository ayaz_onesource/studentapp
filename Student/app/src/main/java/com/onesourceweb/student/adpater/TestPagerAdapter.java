package com.onesourceweb.student.adpater;

import android.content.Context;
import android.nfc.Tag;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.onesourceweb.student.Pager_Onclick;
import com.onesourceweb.student.R;
import com.onesourceweb.student.activity.Assginment;
import com.onesourceweb.student.activity.TestActivity;
import com.onesourceweb.student.model.AssginmentModel;
import com.onesourceweb.student.model.TestModel;
import com.onesourceweb.student.model.TestOptionListModel;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class TestPagerAdapter extends PagerAdapter {

    Context context;
    private RecyclerView mMstudentList;
    private RecyclerView.LayoutManager layoutManager;
    private TestAdpater testAdpater;

    List<TestModel> testModels;
    TextView question;
    Pager_Onclick pagerclick;

    public TestPagerAdapter(TestActivity testActivity, List<TestModel> testModels) {
        this.context = testActivity;
        this.testModels = testModels;
        //this.pagerclick = testActivity;
    }


    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        TestModel testModel = testModels.get(position);


        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.question_iteam, collection, false);
        collection.addView(layout);

        // pagerclick.pagerclick(position);

        mMstudentList = (RecyclerView) collection.findViewById(R.id.mstudentList);
        question = (TextView) collection.findViewById(R.id.question);

        mMstudentList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        mMstudentList.setLayoutManager(layoutManager);
        mMstudentList.setItemAnimator(new DefaultItemAnimator());

      //  testAdpater = new TestAdpater(context, testModel.getOption());
        mMstudentList.setAdapter(testAdpater);

        question.setText(testModel.getQuestion());

        Log.d(TAG, "onadpater: " + testModel.getQuestion() + " data" + testModels.size());

        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {

        return testModels.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {

        return view == (View) object;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
