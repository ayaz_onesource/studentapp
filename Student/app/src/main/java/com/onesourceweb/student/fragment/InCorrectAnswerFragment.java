package com.onesourceweb.student.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onesourceweb.student.R;
import com.onesourceweb.student.adpater.CorrectListAdpater;
import com.onesourceweb.student.adpater.InCorrectListAdpater;
import com.onesourceweb.student.model.QuestionsBean;
import com.onesourceweb.student.model.SummaryCurrentModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InCorrectAnswerFragment extends Fragment {

    private static final String TAG = "InCorrectAnswerFragment";
    static List<QuestionsBean> summaryCurrentModels1=new ArrayList<>();

    public static Fragment newInstance(List<QuestionsBean> summaryCurrentModel) {
        summaryCurrentModels1=summaryCurrentModel;
        InCorrectAnswerFragment inCorrectAnswerFragment = new InCorrectAnswerFragment();
        Bundle arg = new Bundle();
        arg.putSerializable("inc", (Serializable) summaryCurrentModel);
        inCorrectAnswerFragment.setArguments(arg);
        Log.d(TAG, "incorr bundle data: " + arg.size());
        return inCorrectAnswerFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View layoutView = inflater.inflate(R.layout.fragment_correct_answer, container, false);

//        summaryCurrentModels1 = (List<SummaryCurrentModel>) getArguments().getSerializable("inc");

        Log.d(TAG, "inqsize: " + +summaryCurrentModels1.size());

        RecyclerView mMstudentList = (RecyclerView) layoutView.findViewById(R.id.mstudentList);

        mMstudentList.setHasFixedSize(true);
        mMstudentList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mMstudentList.setItemAnimator(new DefaultItemAnimator());
        mMstudentList.setAdapter(new CorrectListAdpater(getActivity(), summaryCurrentModels1));

        return layoutView;
    }
}
