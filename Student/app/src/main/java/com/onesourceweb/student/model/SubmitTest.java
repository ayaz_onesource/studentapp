package com.onesourceweb.student.model;

public class SubmitTest {

    int AnswerID;
    String IsAttempt;

    public int getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(int answerID) {
        AnswerID = answerID;
    }

    public String getIsAttempt() {
        return IsAttempt;
    }

    public void setIsAttempt(String isAttempt) {
        IsAttempt = isAttempt;
    }

    public int getMCQPlannerDetailsID() {
        return MCQPlannerDetailsID;
    }

    public void setMCQPlannerDetailsID(int MCQPlannerDetailsID) {
        this.MCQPlannerDetailsID = MCQPlannerDetailsID;
    }

    public SubmitTest(int answerID, String isAttempt, int MCQPlannerDetailsID) {
        AnswerID = answerID;
        IsAttempt = isAttempt;
        this.MCQPlannerDetailsID = MCQPlannerDetailsID;
    }

    int MCQPlannerDetailsID;
}
