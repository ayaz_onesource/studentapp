package com.onesourceweb.student.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class QuestionsBean implements Serializable {
    /*
    "MCQQuestionID": "2",
        "Question": "qqqq<\/p>\r\n",
        "Options": [
     */
    @JsonField
    int MCQQuestionID;
    @JsonField
    int AnswerID;
    @JsonField
    String Question;
    @JsonField
    List<OptionsBean> Options;

    public int getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(int answerID) {
        AnswerID = answerID;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public List<OptionsBean> getOptions() {
        return Options;
    }

    public void setOptions(List<OptionsBean> options) {
        Options = options;
    }

    public int getMCQQuestionID() {
        return MCQQuestionID;
    }

    public void setMCQQuestionID(int MCQQuestionID) {
        this.MCQQuestionID = MCQQuestionID;
    }
}
