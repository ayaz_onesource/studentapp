package com.onesourceweb.student.adpater;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.onesourceweb.student.R;
import com.onesourceweb.student.model.SummaryCurrentModel;

import java.util.List;

public class InCorrectListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;

    List<SummaryCurrentModel> summaryCurrentModelList;


    public InCorrectListAdpater(Context context, List<SummaryCurrentModel> summaryCurrentModels) {
        this.context = context;
        this.summaryCurrentModelList = summaryCurrentModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_correct_answer_subiteam, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderIn, final int position) {

        final ViewHolder holder = (ViewHolder) holderIn;
        SummaryCurrentModel summaryCurrentModel = summaryCurrentModelList.get(position);
        holder.mMstudentList.setHasFixedSize(true);
        holder.mMstudentList.setLayoutManager(new LinearLayoutManager(context));
        holder.mMstudentList.setItemAnimator(new DefaultItemAnimator());

        holder.mMstudentList.setAdapter(new OptionListAdpater(context, summaryCurrentModel.getOption(), summaryCurrentModel.getAnswerID()));

        holder.mTvquestion.setText(Html.fromHtml(summaryCurrentModel.getQuestionindex() + ") " + summaryCurrentModel.getQuestion()));

    }

    @Override
    public int getItemCount() {
        return summaryCurrentModelList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvquestion;
        RecyclerView mMstudentList;

        public ViewHolder(View v) {
            super(v);
            mTvquestion = (TextView) v.findViewById(R.id.question_summary);
            mMstudentList = (RecyclerView) v.findViewById(R.id.summary_list);

        }

    }

}
