package com.onesourceweb.student.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class LegalDataModel {

    @JsonField(name = "Name")
    String tittle;
    @JsonField(name = "value")
    String subdata;

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getSubdata() {
        return subdata;
    }

    public void setSubdata(String subdata) {
        this.subdata = subdata;
    }

    public LegalDataModel(String tittle, String subdata) {
        this.tittle = tittle;
        this.subdata = subdata;
    }
}
