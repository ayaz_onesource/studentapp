package com.onesourceweb.student.adpater;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.onesourceweb.student.activity.TestActivity;
import com.onesourceweb.student.fragment.TestFragment;
import com.onesourceweb.student.model.TestModel;

import java.util.List;

import static android.content.ContentValues.TAG;

public class TestPagerAdapterNew extends FragmentStatePagerAdapter {

    List<TestModel> testModels;

    public TestPagerAdapterNew(FragmentManager fragmentManager, List<TestModel> testModels) {
        super(fragmentManager);
        this.testModels = testModels;
    }

    @Override
    public int getCount() {
        return testModels.size();

    }

    @Override
    public Fragment getItem(int position) {
       // Log.d(TAG, "aaaaa " + testModels.size() + testModels.get(position).getOption().toString());
        return TestFragment.newInstance(testModels.get(position));
    }

}