package com.onesourceweb.student.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onesourceweb.student.BaseActivity;
import com.onesourceweb.student.R;
import com.onesourceweb.student.adpater.ZookiPagerAdpater;
import com.onesourceweb.student.model.ZookiIteamModel;
import com.onesourceweb.student.network.NetworkRequest;
import com.onesourceweb.student.utils.Constant;
import com.onesourceweb.student.utils.L;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ZookiActivity extends BaseActivity {

    private static final String TAG = "ZookiActivity";
    ArrayList<ZookiIteamModel> zookiIteamModels;

    @BindView(R.id.img_back_zooki)
    ImageView imgBackZooki;
    @BindView(R.id.tv_test_summary)
    TextView tvTestSummary;
    @BindView(R.id.zookipager)
    ViewPager zookipager;

    int qsize;
    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zooki);
        ButterKnife.bind(this);

        zookiIteamModels = new ArrayList<>();

        if (L.isNetworkAvailable(ZookiActivity.this))
            callZooki();

        zookipager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == (zookipager.getAdapter().getCount() - 1)) {

                    tvTestSummary.setText((zookipager.getCurrentItem() - 1) + "/ " + qsize);
                }
                if (position == 0) {
                    tvTestSummary.setText("1" + "/ " + zookiIteamModels.size());

                } else {
                    tvTestSummary.setText((zookipager.getCurrentItem() + 1) + "/ " + qsize);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });
    }


    private void callZooki() {

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getZooki(), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    Log.d(TAG, "zooki" + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());

                    String imgpath = jsonResponse.getString(Constant.Imgfullpath);

                    JSONArray DATA = jsonResponse.getJSONArray("data");

                    for (int i = 0; i < DATA.length(); i++) {

                        JSONObject O = DATA.getJSONObject(i);

                        String ZookiID = O.getString("ZookiID");
                        String Title = (O.getString("Title"));
                        String Description = (O.getString("Description"));
                        String Image = (O.getString("Image"));
                        String CreatedDate = (O.getString("CreatedDate"));
                        String ModifiedDate = (O.getString("ModifiedDate"));

                        ZookiIteamModel zookiIteamModel = new ZookiIteamModel(ZookiID, Title, Description, imgpath + Image, CreatedDate, ModifiedDate);
                        zookiIteamModels.add(zookiIteamModel);
                    }
                    zookipager.setAdapter(new ZookiPagerAdpater(getSupportFragmentManager(), zookiIteamModels));
                    qsize = zookiIteamModels.size();
                    tvTestSummary.setText((zookipager.getCurrentItem() + 1) + "/ " + qsize);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }

    @OnClick(R.id.img_back_zooki)
    public void onViewClicked() {
        startActivity(new Intent(ZookiActivity.this, MainActivity.class));
        finish();
    }
}
