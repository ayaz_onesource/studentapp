package com.onesourceweb.student.adpater;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.onesourceweb.student.OptionsWebView;
import com.onesourceweb.student.R;
import com.onesourceweb.student.model.QuestionsBean;
import com.onesourceweb.student.model.SummaryCurrentModel;

import java.util.List;

public class CorrectListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;

    List<QuestionsBean> summaryCurrentModelList;


    public CorrectListAdpater(Context context, List<QuestionsBean> summaryCurrentModels) {
        this.context = context;
        this.summaryCurrentModelList = summaryCurrentModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_correct_answer_subiteam, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderIn, final int position) {

        final ViewHolder holder = (ViewHolder) holderIn;
        QuestionsBean summaryCurrentModel = summaryCurrentModelList.get(position);
        holder.mMstudentList.setHasFixedSize(true);
        holder.mMstudentList.setLayoutManager(new LinearLayoutManager(context));
        holder.mMstudentList.setItemAnimator(new DefaultItemAnimator());

        holder.mMstudentList.setAdapter(new OptionListAdpater(context, summaryCurrentModel.getOptions(),summaryCurrentModel.getAnswerID()));


        holder.mTvquestion.loadHtmlFromLocal(summaryCurrentModel.getQuestion() + ") " + summaryCurrentModel.getQuestion());
        holder.mTvquestion.setInitialScale(40);

        holder.mTvquestion.setBackgroundColor(Color.TRANSPARENT);
        holder.mTvquestion.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        holder.mTvquestion.setScrollbarFadingEnabled(false);
        holder.mTvquestion.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
    }

    @Override
    public int getItemCount() {
        return summaryCurrentModelList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private OptionsWebView mTvquestion;
        RecyclerView mMstudentList;

        public ViewHolder(View v) {
            super(v);
            mTvquestion = (OptionsWebView) v.findViewById(R.id.question_summary);
            mMstudentList = (RecyclerView) v.findViewById(R.id.summary_list);
        }
    }

}
