package com.onesourceweb.student.adpater;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.onesourceweb.student.List_Onclick;
import com.onesourceweb.student.R;
import com.onesourceweb.student.activity.SearchTestPapers;
import com.onesourceweb.student.activity.TestReportActivity;
import com.onesourceweb.student.model.SearchPaperModel;
import com.onesourceweb.student.utils.Constant;

import java.util.List;

public class SearchPaperAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;

    List<SearchPaperModel> searchPaperModels;
    List_Onclick click;

    public SearchPaperAdpater(SearchTestPapers searchTestPapers, List<SearchPaperModel> searchPaperModels) {
        this.context = searchTestPapers;
        this.searchPaperModels = searchPaperModels;
        this.click = searchTestPapers;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new SearchPaperAdpater.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.search_paper_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        ViewHolder holder = (ViewHolder) viewHolder;
        SearchPaperModel searchPaperModel = searchPaperModels.get(position);
        if (position == 0)
            holder.rowmainlayout.setBackgroundResource(R.drawable.firstpos);
        else if (position == getItemCount() - 1)
            holder.rowmainlayout.setBackgroundResource(R.drawable.lastpos);


        holder.attemppapercount.setText((searchPaperModel.getTotalRight()));
        holder.totalpapercount.setText((searchPaperModel.getTotalQuestion()));
        holder.tvsubname.setText(searchPaperModel.getSubjectName());
        holder.tvdate.setText((searchPaperModel.getSubmitDate()));
        //holder.prog_search_accu.setProgress(Integer.parseInt(searchPaperModel.getProg_search_accu()));
        // holder.tvaccuracy.setText((searchPaperModel.getAccuracy()));


        holder.rowmainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                click.clicklist(searchPaperModel.getStudentMCQTestID());

            }
        });


    }

    @Override
    public int getItemCount() {
        return searchPaperModels.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvsubname, tvdate, tvaccuracy, attemppapercount, totalpapercount;
        private ProgressBar prog_search_accu;
        private LinearLayout rowmainlayout;

        public ViewHolder(View v) {
            super(v);
            tvsubname = (TextView) v.findViewById(R.id.tvsubname);
            tvdate = (TextView) v.findViewById(R.id.tvdate);
            tvaccuracy = (TextView) v.findViewById(R.id.tvaccuracy);
            attemppapercount = (TextView) v.findViewById(R.id.attemppapercount);
            totalpapercount = (TextView) v.findViewById(R.id.totalpapercount);
            prog_search_accu = (ProgressBar) v.findViewById(R.id.prog_search_accu);
            rowmainlayout = (LinearLayout) v.findViewById(R.id.rowmainlayout);

        }

    }
}
