package com.onesourceweb.student.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bluelinelabs.logansquare.LoganSquare;
import com.onesourceweb.student.BaseActivity;
import com.onesourceweb.student.List_Onclick;
import com.onesourceweb.student.R;
import com.onesourceweb.student.adpater.AssginmentAdpater;
import com.onesourceweb.student.model.AssginmentModel;
import com.onesourceweb.student.network.NetworkRequest;
import com.onesourceweb.student.utils.Constant;
import com.onesourceweb.student.utils.L;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class Assginment extends BaseActivity implements List_Onclick {

    private static final String TAG = "Assginment";

    @BindView(R.id.img_back_assignment)
    ImageView imgBackAssignment;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.la)
    LinearLayout la;
    @BindView(R.id.mstudentList)
    RecyclerView mMstudentList;

    List<AssginmentModel> assginmentModels;
    private AssginmentAdpater assginmentAdpater;
    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assginment);
        ButterKnife.bind(this);

        if (L.isNetworkAvailable(Assginment.this))
            callpaperlist();

        assginmentModels = new ArrayList<>();

        mMstudentList.setHasFixedSize(true);
        mMstudentList.setLayoutManager(new LinearLayoutManager(Assginment.this));
        mMstudentList.setItemAnimator(new DefaultItemAnimator());

    }

    private void callpaperlist() {
        Map<String, String> map = new HashMap<>();
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getpaperlist(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    Log.d(TAG, "assginmentlist " + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONArray DATA = jsonResponse.getJSONArray("data");
                    assginmentModels = LoganSquare.parseList(DATA.toString(), AssginmentModel.class);

                    assginmentAdpater = new AssginmentAdpater(this, assginmentModels);
                    mMstudentList.setAdapter(assginmentAdpater);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }


    @Override
    public void clicklist(int id) {

        // Toast.makeText(this, "" + id, Toast.LENGTH_SHORT).show();
        Intent i = new Intent(Assginment.this, TestActivity.class);
        i.putExtra(Constant.MCQPlannerID, id);
        startActivity(i);
    }

    @OnClick({R.id.img_back_assignment, R.id.img_search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back_assignment:
                startActivity(new Intent(Assginment.this, MainActivity.class));
                finish();
                break;
            case R.id.img_search:
                break;
        }
    }
}
