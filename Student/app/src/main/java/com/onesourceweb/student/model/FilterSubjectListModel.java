package com.onesourceweb.student.model;

public class FilterSubjectListModel {
    public FilterSubjectListModel(String subjectname, String subjectID) {
        this.subjectname = subjectname;
        SubjectID = subjectID;
    }

    String subjectname, SubjectID;

    public String getSubjectname() {
        return subjectname;
    }

    public void setSubjectname(String subjectname) {
        this.subjectname = subjectname;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }
}
