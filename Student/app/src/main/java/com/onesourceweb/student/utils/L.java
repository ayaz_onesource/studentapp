package com.onesourceweb.student.utils;

import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.service.autofill.UserData;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.onesourceweb.student.BuildConfig;
import com.onesourceweb.student.R;
import com.onesourceweb.student.StudentApplication;
import com.onesourceweb.student.activity.LoginActivity;
import com.onesourceweb.student.listner.DialogButtonListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import retrofit2.Response;

public class L {

    static boolean isShowing = false;
    ProgressDialog progressDialog;

    //Get Device Id
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

    }

    //For Network Available
    public static boolean isNetworkAvailable(Context context) {
        boolean isConnected = false;
        if (!Constant.isAlertShow) {
            if (!isNetworkConnected(context)) {
                showTwoButtonDialog(context, context.getString(R.string.network_error_title), context.getString(R.string.network_connection_error), "Ok", null, new DialogButtonListener() {
                    @Override
                    public void onPositiveButtonClicked() {
                        Constant.isAlertShow = false;
                    }

                    @Override
                    public void onNegativButtonClicked() {
                        Constant.isAlertShow = false;
                    }
                });
                Constant.isAlertShow = true;
                isConnected = false;
            } else
                isConnected = true;
        }
        return isConnected;
    }

    public static boolean isNetworkConnected(Context c) {
        boolean isConnected = false;
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            switch (activeNetwork.getType()) {
                case ConnectivityManager.TYPE_WIFI:
                    // connected to wifi
                    isConnected = true;
                    break;
                case ConnectivityManager.TYPE_MOBILE:
                    // connected to mobile data
                    isConnected = true;
                    break;
                default:
                    break;
            }
        } else {
            return false;
        }
        return isConnected;
    }

    public static void showTwoButtonDialog(Context context, String title, String message, String yesButtonName, String noButtonName, DialogButtonListener dialogButtonListener) {

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
        if (title != null)
            alertDialogBuilder.setTitle(title);
        else {
            alertDialogBuilder.setTitle("");
        }
        alertDialogBuilder
                .setMessage(Html.fromHtml(message))
                .setCancelable(false);
        if (yesButtonName != null) {
            yesButtonName = yesButtonName.equals("") ? "YES" : yesButtonName;
            alertDialogBuilder.setPositiveButton(yesButtonName, (dialog, id) -> {
                dialog.cancel();
                if (dialogButtonListener != null) {
                    dialogButtonListener.onPositiveButtonClicked();
                }
            });
        }

        if (noButtonName != null) {
            noButtonName = noButtonName.equals("") ? "NO" : noButtonName;
            alertDialogBuilder.setNegativeButton(noButtonName, (dialog, id) -> {
                dialog.cancel();
                if (dialogButtonListener != null) {
                    dialogButtonListener.onNegativButtonClicked();
                }
            });
        }
        // create alert dialog
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    public static void serviceStatusFalseProcess(Context context, Response<String> data) {
        try {
           /* if (data.code() == 401) {
                L.logout(context);
            } else */
            if (data.code() == 500) {
                L.generalOkAlert(context, data.message(), data);
            } else {
                String msg;
                String str = data.errorBody().string();
                JSONObject jsonResponse = new JSONObject(str);
//                if (jsonResponse.has(Constant.messageAr) && L.isArabicLanguage(context)) {
//                    msg = jsonResponse.getString(Constant.messageAr);
//                } else {
                msg = jsonResponse.getString(Constant.message);

                L.generalOkAlert(context, msg, data);
            }

        } catch (Exception e) {
            e.printStackTrace();
            L.generalOkAlert(context, "oops!\nsomething went wrong.\nPlease try again!", null);
            e.printStackTrace();
        }
    }

    public static void generalOkAlert(Context context, String message, Response<String> data) {
        if (context == null)
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setPositiveButton(context.getResources().getString(R.string.ok), (dialog, which) -> {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    dialog.dismiss();
                    isShowing = false;
                    if (data != null && data.code() == 401)
                        L.logout(context);
                    break;
            }
        });
        if (!isShowing) {
            builder.show();
            isShowing = true;
        }

    }

    public static void logout(Context context) {
        Prefs prefs = Prefs.with(context);
        prefs.save(Constant.isLogin, false);
//        prefs.save(Constant.USER_DATA, null);

        Intent intent = new Intent(context, LoginActivity.class);
//        intent.putExtra(Constant.showLoginError, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static String getAuthtoken(Context context) {
        return Prefs.with(context).getString(Constant.loginAuthToken, "");
    }

    public static String getUser(Context context) {
        return Prefs.with(context).getString(Constant.USERID, "");
    }

    public static boolean isLogin(Context context) {
        return Prefs.with(context).getBoolean(Constant.isLogin, false);
    }

    public static void print(String msg) {
        String TAG = "Student";
        if (BuildConfig.DEBUG)
            Log.d(TAG, msg);
    }

    public static String getEditText(EditText editText) {
        return editText.getText().toString().trim();
    }

    public static String getText(TextView txtView) {

        return txtView.getText().toString().trim();
    }

    public static void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

    }

    public static void loadImageWithPicasso(Context context, String imagePath, ImageView iv, ProgressBar mProgress) {
        if (!imagePath.isEmpty()) {

            if (mProgress != null)
                mProgress.setVisibility(View.VISIBLE);
            Picasso.with(context)
                    .load(imagePath)
                    .error(R.drawable.logo).into(iv, new Callback() {
                @Override
                public void onSuccess() {
                    if (mProgress != null)
                        mProgress.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    if (mProgress != null)
                        mProgress.setVisibility(View.GONE);
                }
            });

        } else
            Picasso.with(context).load(R.drawable.logo).into(iv);
    }

    public static String convertUTF8EncodedStringToNormalString(String utf8EncodedString)
    {
        /*utf8EncodedString = "\u0041\u0041\u0041\u0041\u0041";*/

        String normal = "";

        if(utf8EncodedString!=null)
            try
            {
                byte[] b = utf8EncodedString.getBytes("UTF-8");
                normal = new String(b);

            } catch (Exception e)
            {
                if(e!=null)
                    e.printStackTrace();
            }

        return normal;
    }

}
