package com.onesourceweb.student.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.onesourceweb.student.BaseActivity;
import com.onesourceweb.student.R;
import com.onesourceweb.student.network.NetworkRequest;
import com.onesourceweb.student.utils.Constant;
import com.onesourceweb.student.utils.L;
import com.onesourceweb.student.utils.Validation;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;

public class QueryActivity extends BaseActivity {

    Subscription subscription;
    private ImageView mImgBackReport;
    private MaterialEditText mEdtId;
    private Button mBtnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query);
        initView();

        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Validation.isEmpty(mEdtId.getText().toString().trim())) {
                    mEdtId.setError("Please enter Student Id");
                } else {
                    if (L.isNetworkAvailable(QueryActivity.this))
                        callFeedback();
                }
            }
        });
        mImgBackReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(QueryActivity.this, MainActivity.class));
                finish();
            }
        });
    }

    private void callFeedback() {
        Map<String, String> map = new HashMap<>();
        map.put("Message", mEdtId.getText().toString().trim());
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.sendfeedback(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    mEdtId.setText("");
                    L.generalOkAlert(this, jsonResponse.getString("message"), data);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });
    }

    private void initView() {
        mImgBackReport = (ImageView) findViewById(R.id.img_back_report);
        mEdtId = (MaterialEditText) findViewById(R.id.edt_id);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
    }
}
